import React from 'react'
import "./Header.css"
import { Link,useNavigate } from 'react-router-dom';
import { Container, Row, Col } from 'react-grid-system';
import images from "../header/images.png"
import cart from '../header/cart.png'
import Cookies from 'js-cookies'
import { getCookie } from '../../helpers/Helpers';


const CustomerHeader = ({count}) => {
  console.log(count)
    let navigate = useNavigate();
    function removeCookie(){
        Cookies.removeItem('auth_cookies')
      }
      let user = getCookie()
    //  console.log("gggdgd")
      
      return (
        <div className='header'>
           
     
            <Container>
      <Row  className='row'>
        <Col className='col1' sm={6}> <h2>{user.first_name}</h2></Col>
        <Col className='col2'  sm={6}><img onClick={() => navigate('/customer/cartlist')} className='cart' alt='' src={cart} height={30} width={30}/><span className='badge'>{count}</span><img className='avatar' alt='' src={images} height={30} width={30} /></Col>
      </Row>
      <div className="dropdown">
      <button className="dropbtn"></button>
      <div className="dropdown-content">
      <Link  to='/signin' style={{ textDecoration: 'none' }} onClick={removeCookie}>Log out</Link>
      <Link  to='/customer/cartlist' style={{ textDecoration: 'none' }} >My Cart</Link>
      <Link  to='/customer/orderlist' style={{ textDecoration: 'none' }} >My Order</Link>
       </div>
    </div>
     
    </Container>
    
            </div>
      )
    }

export default CustomerHeader