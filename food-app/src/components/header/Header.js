import React from 'react'
import "./Header.css"
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-grid-system';
import images from "../header/images.png"
import Cookies from 'js-cookies'
import { getCookie } from '../../helpers/Helpers';

function Header() {
  function removeCookie() {
    Cookies.removeItem('auth_cookies')
  }
  let user = getCookie()
  return (
    <div className='header'>
      <Container>
        <Row className='row'>
          <Col className='col1' sm={6}> <h2>{user.first_name}</h2></Col>
          <Col className='col2' sm={6}><img className='avatar' alt='' src={images} height={30} width={30} /></Col>
        </Row>
        <div className="dropdown">
          <button className="dropbtn"></button>
          <div className="dropdown-content">
            <Link to='/signin' style={{ textDecoration: 'none' }} onClick={removeCookie}>Log out</Link>
          </div>
        </div>

      </Container>
    </div>
  )
}

export default Header