import React from 'react';
import { TextField, MenuItem } from '@material-ui/core';
import { useField, useFormikContext } from 'formik';
import { Input } from '@mui/material';


const ImageWrapper = ({
  name,
  ...otherProps
}) => {
  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(name);

  const handleChange = evt => {
    const { value } = evt.currentTarget.files[0];
    setFieldValue(name, value);
    console.log(setFieldValue)
  };

  const configSelect = {
    ...field,
    ...otherProps,
    variant: 'outlined',
    fullWidth: true,
    onChange: handleChange
  };

  if (meta && meta.touched && meta.error) {
    configSelect.error = true;
    configSelect.helperText = meta.error;
  }

  return (
    <input   {...configSelect}
                  id="image"
                  name="image"
                  type="file"
                  multiple={true}
                  onChange={(e) => {handleChange}}
                />
  );
};

export default ImageWrapper;