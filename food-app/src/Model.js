import authModel from './containers/auth/Model';
import userModel from './containers/users/Model';
import productModel from './containers/products/Model'
import categoryModel from './containers/categories/Model'
import roleModel from './containers/roles/Model'
import cartModel from './containers/cart/Model'
import orderModel from './containers/order/Model'

export  {authModel,userModel,productModel,categoryModel,roleModel,cartModel,orderModel};

