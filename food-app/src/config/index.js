let server="http://localhost:5009";
const all={
    routes:{}
};
const env={
    development:{
        api:server
    },
    staging:{
        api:server
    },
    production:{
        api:server,

    },
};
export default{
    ...all,
    ...env[process.env.NODE_ENV],

};