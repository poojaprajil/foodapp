import React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";


import Product from '../containers/products/Product';
import AddProduct from '../containers/products/AddProduct'
import UpdateProduct from '../containers/products/UpdateProduct';
import Signup from '../containers/auth/Signup';
import UserView from '../containers/users/UserView';
import Login from '../containers/auth/Login';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';
import Category from '../containers/categories/Category';
import AddUser from '../containers/users/AddUser'
import UpdateUser from '../containers/users/UpdateUser'
import Home from '../containers/CustomerDashboard/Home';
import ProductList from '../containers/CustomerDashboard/ProductList';
import CartList from '../containers/CustomerDashboard/CartList';
import OrderList from '../containers/CustomerDashboard/OrderList';
import ForgotPassword from '../containers/auth/ForgotPassword';
import ResetPassword from '../containers/auth/ResetPassword';


function UserRoute() {

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<Navigate to={{ pathname: '/signin' }} />} />
          <Route path="/" element={<PublicRoute component={Login} />} />
          <Route path="/signup" element={<PublicRoute component={Signup} />}></Route>
          <Route path="/signin" element={<PublicRoute component={Login} />}></Route>
          <Route path="/product" element={<PrivateRoute component={Product} />}></Route>
          <Route path="/category" element={<PrivateRoute component={Category} />}></Route>
          <Route path="/addproduct" element={<PrivateRoute component={AddProduct} />}></Route>
          <Route path="/updateproduct/:productId" element={<PrivateRoute component={UpdateProduct} />}></Route>
          <Route path="/admin/user" element={<PrivateRoute component={UserView} />}></Route>
          <Route path="/admin/adduser" element={<PrivateRoute component={AddUser} />}></Route>
          <Route path="/admin/updateuser/:userId" element={<PrivateRoute component={UpdateUser} />}></Route>
          <Route path="/customer/home" element={<PrivateRoute component={Home} />}></Route>
          <Route path="/customer/productlist" element={<PrivateRoute component={ProductList} />}></Route>
          <Route path="/customer/cartlist" element={<PrivateRoute component={CartList} />}></Route>
          <Route path="/customer/orderlist" element={<PrivateRoute component={OrderList} />}></Route>
          <Route path="/forgotpassword" element={<PublicRoute component={ForgotPassword} />}></Route>
          <Route path="/resetpassword/:token" element={<PublicRoute component={ResetPassword} />}></Route>




        </Routes>
      </BrowserRouter>

    </div>
  )
}

export default UserRoute;