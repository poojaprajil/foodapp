import React from 'react';
import { getCookie } from '../helpers/Helpers';
import { Navigate } from 'react-router-dom';

function PublicRoute({ component: Component }) {
    const auth_cookies = getCookie()
    return (
        !auth_cookies ? <Component /> : (auth_cookies.role_id === 1 ? <Navigate to={{ pathname: '/customer/home' }} /> : <Navigate to={{ pathname: '/admin/user' }} />)


    )
}
export default PublicRoute;
