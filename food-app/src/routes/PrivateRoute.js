import React from 'react';

import { Navigate } from 'react-router-dom';
import { getCookie } from '../helpers/Helpers';

function PrivateRoute({ component: Component }) {
    const auth_cookies = getCookie()
    return (
        auth_cookies ? <Component /> : <Navigate to={{ pathname: '/signin' }} />

    )
}
export default PrivateRoute;
