import cookies from 'js-cookies'



export function saveCookies(data) {
  if (data.token) {
    let cookieData = JSON.stringify(data)
    cookies.setItem("auth_cookies", cookieData)
    // return cookieData
  }

}

export function getCookie() {
  const cook = cookies.getItem("auth_cookies")
  if (cook) {
    const decode = JSON.parse(cook)
    //   const first=decode.first_name
    return decode
  }

}

