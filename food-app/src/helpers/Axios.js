import axios from 'axios';
import config from '../config';
import cookies from 'js-cookies'
function CustomHeader() {
    // const cookies = new Cookies();
    let header = {
        "content-Type": "application/json",
        Accept: "application/json,"
    }
    if (cookies.getItem("auth_cookies")) {
        let currentUser = cookies.getItem("auth_cookies")
        // console.log(JSON.parse(currentUser).token)
        header["x-access-token"] = JSON.parse(currentUser).token
    }
    return header
}
function newHeader() {
    let header = {
        "content-Type": "multipart/form-data",
        Accept: "application/json,"
    }
    if (cookies.getItem('auth_cookies')) {
        let currentUser = cookies.getItem("auth_cookies")
        header["x-access-token"] = JSON.parse(currentUser).token
    }
    return header
}

export function api() {
    let opts = {
        baseURL: config.api.trim(),
        headers: CustomHeader(),
    };
    return axios.create(opts);
}
export function newApi (){
    let opts = {
        baseURL: config.api.trim(),
        headers: newHeader(),
    };
    return axios.create(opts);
 
}
