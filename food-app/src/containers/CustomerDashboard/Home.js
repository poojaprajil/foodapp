import React from 'react'
import CustomerHeader from '../../components/header/CustomerHeader';
import CustomerSidebar from '../../components/sidebar/CustomerSidebar'

const Home = () => {
  return (
    <div>
        <CustomerHeader/>
        <CustomerSidebar/>
    <h1 className='heading'>Welcome </h1>
    </div>
  )
}

export default Home