import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import CustomerHeader from '../../components/header/CustomerHeader';
import CustomerSidebar from '../../components/sidebar/CustomerSidebar'
import './ProductList.css'
import { getCookie } from '../../helpers/Helpers';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const ProductList = () => {

  const dispatch = useDispatch();
  const [products, setProducts] = useState([])
  const [cartData, setCartData] = useState([])
  const [counter,setCounter] = useState([])
  let cartCount=0
  let obj = {}
  const user = getCookie()
  let params = {
    user_id: user.id
  }
  const notify = () => {
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }
  useEffect(() => {

    listProducts()
  }, [])
  async function listProducts() {
    const product = await dispatch.productModel.getProduct();
    const cartDatas = await dispatch.cartModel.getCartData(params);
    setCartData(cartDatas)
    if (product.rows && cartDatas.rows) {
      product.rows.forEach((item) => {
        cartDatas.rows.forEach((cartItem) => {
          if (item.id == cartItem.product_id && cartItem.user_id == user.id) {
            item.count = cartItem.count
          }
        })
      })
      setProducts(product.rows)
    }
    else {
      setProducts(product.rows)
    }
  }
  if(cartData.count>0){
    cartData.rows.map((item, index) => {
       cartCount=cartCount+item.count
     })
  }

  const incrementCount = async (product, index) => {
    let cartData = {
      product_id: product.id,
      product_name: product.name,
      count: product.count + 1,
      user_id: user.id
    }
    const data = await dispatch.cartModel.addToCart(cartData);
    listProducts()
  }
  const decrementCount = async (product, index) => {
    let cartData = {
      product_id: product.id,
      product_name: product.name,
      count: product.count - 1,
      user_id: user.id
    }
    const data = await dispatch.cartModel.addToCart(cartData);
    listProducts()
  }


  const addToCart = async (product) => {
    let cartData = {
      product_id: product.id,
      product_name: product.name,
      count: 1,
      user_id: user.id
    }
    const data = await dispatch.cartModel.addToCart(cartData);
    if (data) {
      notify()
      listProducts()
    }
    return (data);
  };

  return (
    <div>
      <CustomerHeader count={cartCount} />
      <CustomerSidebar />
      <div className='product'>
        <h1>Product List</h1>

        <ul > {products.map((item, index) => {
          return (<li className='list' key={index}>
            <div className='containerDiv'>
              <img className='img1' src={`http://localhost:5009/${item.image}`} alt={item.name} />
              <div className='items'>
                <h3 className='name' align="left">{item.name}</h3>
                <p className='price' align="left">Rs: {item.price}</p>
                <p className='description' align="left">{item.description}</p>
              </div>
              {(!item.count || item.count < 1) ?
                <div>
                  <button className='addtocart' onClick={() => { addToCart(item) }} >Add to Cart</button>
                </div> :
                <div className="buttons">
                  <button className='addtocart1' style={{ border: "1px solid", padding: '1px 10px' }} onClick={() => decrementCount(item, index)}>-</button>
                  {item.count}
                  <button className='addtocart1' style={{ border: "1px solid", padding: '1px 10px' }} onClick={() => incrementCount(item, index)}>+</button>

                </div>
              }

            </div>
          </li>
          )
        })}
        </ul>
      </div>

    </div>
  )
}

export default ProductList
