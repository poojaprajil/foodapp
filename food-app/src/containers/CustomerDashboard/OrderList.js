import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import './OrderList.css'
import { getCookie } from '../../helpers/Helpers';
import { Link } from 'react-router-dom';

const OrderList = () => {
  const dispatch = useDispatch();
  const [orderDatas, setOrderDatas] = useState([])
  const user = getCookie()
  let params = {
    user_id: user.id
  }
  useEffect(() => {
    async function listOrder() {
      const orderDatas = await dispatch.orderModel.getOrderData(params);
      setOrderDatas(orderDatas.rows)
    }
    listOrder()
  }, [])



  return (
    <div>
      <button className="fa" style={{ fontSize: "40px" }} >
        <Link to="/customer/productlist" style={{ textDecoration: 'none', color: 'black' }}>&#xf0a8;</Link>
      </button>
      <h2 className='head1'> My Order</h2>
      <table className='table4'>
        <thead className='borderBottom' >
          <tr className='tr4'>
            <td className='td41'>Reference Id</td>
            <td className='td41'>Name</td>
            <td className='td41'>Quantity</td>
            <td className='td41'>Amount</td>

          </tr>
        </thead>

        <tbody>
          {orderDatas && orderDatas.length ? orderDatas.map((item, index) =>
            <tr key={index} className='tr4'>
              <td className='td4'>{item.reference_id}</td>
              <td className='td4'>{item.product.name}</td>
              <td className='td4'>{item.item_count}</td>
              <td className='td4'> &#8377;{item.amount}</td>
            </tr>
          ) : <tr><td style={{ color: "blue" }}>No items here</td></tr>}
        </tbody>
      </table>
      {/* <h2> Order List</h2> 
        <ul > {orderDatas && orderDatas.length ?orderDatas.map((item, index) => {
        return (<li className='list' key={index}>
          <div className='containerDiv'>
            <img  className='img' src={`http://localhost:5009/${item.product.image}`} alt={item.name} />
            <div className='items'>
              <h3 className='reference' align="left">{item.product.name}</h3>
              <p className='name' align="left">Reference Id:{item.reference_id} </p>
              <p  className='count' align="left"> Qty: {item.item_count}</p>
              <p className='amount' align="left">&#8377;:{item.amount}</p>

            </div>
         
          </div>
        </li>
        
        )
      }
     
      )
      : <p style={{ color: "blue" }}>No items here</p>}

      </ul>
       */}

    </div>

  )
}

export default OrderList