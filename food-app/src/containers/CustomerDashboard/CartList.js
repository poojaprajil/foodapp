import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import './CartList.css'
import { getCookie } from '../../helpers/Helpers';
import { confirm } from "react-confirm-box";
import { useNavigate, Link } from 'react-router-dom';
import "./CustomDashboard.css"
import CustomerHeader from '../../components/header/CustomerHeader';
import ProductList from './ProductList';


const CartList = () => {
  const dispatch = useDispatch();
  const [cartDatas, setCartDatas] = useState([])
  const [counter, setCounter] = useState([])
  const [totalCount, setTotalCount] = useState([])
  const [totalPrice, setTotalPrice] = useState([])


  let priceItem = 0, countItem = 0
  let userData = getCookie()
  const confirmbuttons = {
    labels: {
      confirmable: "Confirm",
      cancellable: "Cancel"
    }
  }
  let obj = {}
  const navigate = useNavigate()
  useEffect(() => {

    listCart({ user_id: userData.id })
  }, [])
  async function listCart(params) {
    const cartDatas = await dispatch.cartModel.getCartData(params);
    if (cartDatas) {
      setCartDatas(cartDatas.rows)
      cartDatas.rows.map((item, index) => {
        obj[`${item.product_id}`] = item.count;
        setCounter({ ...counter, ...obj })
        countItem = countItem + item.count
        priceItem = priceItem + item.count * item.product.price;
        setTotalCount(countItem)
        setTotalPrice(priceItem)
      })
    }
    else {
      alert("cart is empty")
    }

  }
  const incrementCount = async (product, index) => {
    let cartData = {
      product_id: product.product_id,
      product_name: product.product_name,
      count: product.count + 1,
      user_id: userData.id
    }
    const data = await dispatch.cartModel.addToCart(cartData);
    listCart({ user_id: userData.id })
  }
  const decrementCount = async (product, index) => {
    let cartData = {
      product_id: product.product_id,
      product_name: product.product_name,
      count: product.count - 1,
      user_id: userData.id
    }
    const data = await dispatch.cartModel.addToCart(cartData);
    window.location.reload(false);
  }

  async function removeFromCart(cartItem) {
    let params = {
      user_id: userData.id,
      product_id: cartItem.product_id
    }
    const result = await confirm(`Are you sure to delete product  ${cartItem.product_name}`, confirmbuttons);
    if (result) {
      console.log("You click yes!");
      await dispatch.cartModel.removeCart(params);
      window.location.reload(false);
    }
  }

  const cartOrder = async (e) => {
    const reference = Math.random().toString(36).slice(-8);
    const res = cartDatas.map((item, index) => {
      return {
        reference_id: reference,
        user_id: item.user_id,
        product_id: item.product_id,
        item_count: item.count,
        amount: item.product.price * item.count
      }
    })
    await dispatch.orderModel.orderCart(res)
    navigate('/customer/orderlist')
  }


  return (
    <div>
      <button className="fa" style={{ fontSize: "40px" }} >
        <Link to="/customer/productlist" style={{ textDecoration: 'none', color: 'black' }}>&#xf0a8;</Link>
      </button>
      <h2 className='head1'> Cart List</h2>
      <table border="4" className='table3'>
        <thead className='borderBottom' >
          <tr>
            <td className='td3'>Product</td>
            <td className='td3'>Price</td>
            <td className='td3'>Quantity</td>
            <td className='td3'>Amount</td>
            <td className='td3'>Action</td>
          </tr>
        </thead>

        <tbody>
          {cartDatas && cartDatas.length ? cartDatas.map((item, index) =>
            <tr key={index} className='tr1'>
              <td className='td3'><img className='img2' src={`http://localhost:5009/${item.product.image}`} alt={item.name} /><br />{item.product_name}</td>
              <td className='td3'>&#8377;{item.product.price}</td>
              <td className='td3'> <button className='button1' style={{ border: "1px solid", padding: '1px 10px' }} onClick={() => decrementCount(item, index)}>-</button>
                {item.count}
                <button className='button1' style={{ border: "1px solid", padding: '1px 10px' }} onClick={() => incrementCount(item, index)}>+</button></td>
              <td className='td3'> &#8377;{(item.product.price) * (item.count)}</td>
              <td><button className="fa fa-trash trashclr" onClick={() => { removeFromCart(item) }} ></button></td>
            </tr>
          ) : <tr style={{ color: "blue" }}><td>No items here</td></tr>}
          <tr className='borderTop'>
            <td className='total' colSpan="5">
              Total Qty: {(totalCount)}
              <br />
              Total Amount: &#8377;{(totalPrice)}<br />
              <button className='buy' onClick={cartOrder}>Place Order</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default CartList