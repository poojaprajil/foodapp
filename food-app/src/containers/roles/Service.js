import {api} from '../../helpers/Axios'

export function roleView(){    
  
   return api().get(`/api/role/getRoles`)
   .then(res =>{    
       return res.data})
    .catch((e)=>console.log("Error",e))
        }