import * as service from './Service';


export default {
    state: {
        role :[]
    },
    reducers: {
        setRoleDetails: (state, data) => {
            
            return {
                ...state,...data
            };
        },
    },
    effects: {
        async getRoles(params,state) {      
            try {
                let res = await service.roleView(params);
                this.setRoleDetails(res);
                return res;
            }
            catch (e) {
                console.log(e)
            }
        }
    }
};
