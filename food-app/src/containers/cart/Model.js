import * as service from './Service';
import { toast } from 'react-toastify';

export default {
    state: {
        cart :[]
    },
    reducers: {
        setCartDetails: (state, data) => {
            
            return {
                ...state,...data
            };
        },
        displayError: (state, error) => {
            return toast(' 🚀' + error, {
                position: "top-right",
            });

           
        },
    },
    effects: {
        async getCartData(params,state) {      
            try {
                let res = await service.cartView(params);
                this.setCartDetails(res);
                if (res.message !== "true") {
                    this.displayError(res.message)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async addToCart(payload,state) {      
            try {
                let res = await service.createCartData(payload);
                this.setCartDetails(res);
                if (res.message !== "true") {
                    this.displayError(res.message)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async removeCart(payload) {
            try {
                let res = await service.deleteCartData(payload);
                this.setCartDetails(res);
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async editCartData(payload) {
                
            try {
                let res = await service.updateCart(payload);
                this.setCartDetails(res);
                return res 
                
            }
            catch (e) {
                console.log(e)
            }
        }

  
    }
};
