import { api } from '../../helpers/Axios'

export function cartView(params) {
    let param = new URLSearchParams(params).toString();
    return api().get('/api/cart/listcart?' + param)
        .then(res => {
            return res.data
        })
        .catch((e) => console.log("Error", e))
}

export function createCartData(product) {
    return api().post('/api/cart/addtocart', product)
        .then((res) => {
            return res
        })
        .catch((e) => console.log("Error", e))
}

export async function deleteCartData(params) {
    let param = new URLSearchParams(params).toString();
    return api().delete('api/cart/deletecartitem?' + param)
        .then((res) => res.data)
        .catch((e) => console.log("Error", e))
}
export function updateCart(params) {
    return api().put('/api/cart/updatecartitem', params)
        .then((res) => res.data)
        .catch((e) => console.log("Error", e))
}


