import { api, newApi } from '../../helpers/Axios'

export function productView(params) {
    let param = new URLSearchParams(params).toString();
    return api().get('/api/product/listproducts?' + param)
        .then(res => {
            return res.data
        })
        .catch((e) => console.log("Error", e))
}

export function createProduct(product) {
    return newApi().post('/api/product/addproduct', product)
        .then((res) => {
            return res
        })
        .catch((e) => console.log("Error", e))
}

export async function deleteProduct(id) {
    return api().delete(`/api/product/deleteproduct?id=${id}`)
        .then((res) => res.data)
        .catch((e) => console.log("Error", e))
}
export function updateProduct(product) {
    return newApi().put(`/api/product/updateproduct?id=${product.id}`, product.value)
        .then((res) => res.data)
        .catch((e) => console.log("Error", e))
}


