import React, { useEffect, useState } from 'react';
import Header from '../../components/header/Header'
import Sidebar from '../../components/sidebar/Sidebar'
import { Formik, ErrorMessage } from 'formik';
import { useNavigate} from "react-router-dom";
import Select from 'react-select';
import { useDispatch } from 'react-redux';
import * as Yup from "yup";
import './AddProduct.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AddProduct = () => {
  const [category, setCategoy] = useState([]);
  const[image,setImage]=useState()
  let navigate = useNavigate();
  const dispatch = useDispatch();
  let [product, setProduct] = useState({});
  const notify = () => { 
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }
  useEffect(() => {
    async function fetchCategory() {
      var categoryName =  await dispatch.categoryModel.getCategory();
      const options = [];
      categoryName.map((item) => {
        return (
          options.push({ value: item.id, label: item.name }))
      })
      setCategoy(options);
    }
    fetchCategory();
  }, [])
  const handleSubmit = async value => {
    const res=  await dispatch.productModel.addProduct(value);
    if(res.data.message==true){
    navigate('/product')
  }
  else{
   notify()
  }
 
  }
  return (
    <div>  <Header />
    <div > <Sidebar/></div> 
    <div className="addproducts">
      <div className="adddiv"><h1>ADD PRODUCT</h1></div>
        <Formik
          initialValues={{
            name:'',
            description:'',
            category_id:'',
            price:'',
            image:{}
          }}

          validationSchema={
            Yup.object().shape({
              name: Yup.string().required("Required"),
              description: Yup.string().required("Required"),
              category_id: Yup.string().required("Required"),
              price: Yup.string().required("Required")
            })
          }
          onSubmit={(values) => {
            handleSubmit(values)
          }}
        >
          {({ values,
           handleSubmit,
           handleChange,
           handleBlur,
           setFieldValue
             }) => (
              <form className="clearfix1" method="POST" onSubmit={handleSubmit} noValidate>
              <ErrorMessage name="name"  component="span" className="form-error" />
              <input type="text" className="control1"
                placeholder="Name"
                name="name"
                id="name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
              />
              <br></br><br></br>
              <ErrorMessage name="description" component="span" className="form-error" />
              <textarea className="area" 
                placeholder="Description"
                name="description"
                id="description"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.description}
              />
              <br></br>
              <ErrorMessage name="category_id" component="span" className="form-error" />
              <Select className="control2" 
                name="category_id"
                id="category_id"
                placeholder="category id"            
                options={category}
                value={values.optionChange}           
                onChange={value =>setFieldValue('category_id', value.value)} 
              />
              <br></br>
              <ErrorMessage name="price" component="span" className="form-error" />
              <br></br>
              <input type="text" className="control1"
                placeholder="price" 
                name="price"
                id="price"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.price}
              />
              <br></br>
              <label className='box'>Upload Image</label>
                    <input  type="file" className="box"
                      name="image"
                      id="image"
                      onChange={(e) => {
                        setFieldValue("image",e.target.files[0])
                      }}
                      onBlur={handleBlur}
                    />
                    <br/><br/>
              <button type="submit" className="registerbtn">Add</button>
              <input type="button" className="cancel" value="Cancel" onClick={() => navigate('/product')} />
            </form>
          )}
        </Formik>

      </div>
      
    </div>
  )
}



export default AddProduct