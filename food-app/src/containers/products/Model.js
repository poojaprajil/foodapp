import * as service from './Service';


export default {
    state: {
        product: []
    },
    reducers: {
        setProductDetails: (state, data) => {

            return {
                ...state, ...data
            };
        },
    },
    effects: {
        async getProduct(params, state) {
            try {
                let res = await service.productView(params);
                this.setProductDetails(res);
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async addProduct(payload, state) {
            try {
                let res = await service.createProduct(payload);
                this.setProductDetails(res);
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async removeProduct(payload) {
            try {
                let res = await service.deleteProduct(payload);
                this.setProductDetails(res);
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async editProduct(payload) {

            try {
                let res = await service.updateProduct(payload);
                this.setProductDetails(res);
                return res

            }
            catch (e) {
                console.log(e)
            }
        }


    }
};
