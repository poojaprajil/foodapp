import React,{ useEffect, useState } from 'react'
import {  useDispatch } from 'react-redux';
import "./Product.css"
import Header from '../../components/header/Header';
import Sidebar from '../../components/sidebar/Sidebar';
import ReactPaginate from 'react-paginate';
import { Link,useNavigate} from 'react-router-dom';
import { confirm } from "react-confirm-box";


function Product() {

  const [name,setName]=useState([])
  const [pageCount, setpageCount] = useState(0);
  const [category,setCategory]=useState([])
  const [categoryId,setCategoryId]=useState("")
  const[statusValue,setStatusValue]=useState("")
  const [search,setSearch]=useState("")
  // const [offset,setOffset]=useState(0)
  const dispatch = useDispatch();
  let navigate = useNavigate()
  const status= [
    { value:'active'},
    { value:'inactive'},
    { value:'trash'}
  ]
  const confirmbuttons = {
    labels: {
        confirmable: "Confirm",
        cancellable: "Cancel"
    }
}


  const limit=5
  let offset=0
  let params={
    offset:0,limit:limit,category_id:categoryId,status:statusValue,search:""
  }  
  useEffect( () => {
      async function listProducts() { 
        fetchProducts(params)         
        }
    listProducts()
    fetchCategory()
    }, [])
  
  const fetchProducts = async (params) => {   
    const data = await dispatch.productModel.getProduct(params); 
    const total = data.count;
    setpageCount(Math.ceil(total / limit));
    setName(data.rows)

  };
  const handlePageClick = async (data) => {
       let offset1=data.selected*limit
       let offset=offset1
       let params={offset:offset,limit:limit,category_id:categoryId,status:statusValue,search:search}
       fetchProducts(params)
     };
     const fetchCategory = async () => {   
      const category1 = await dispatch.categoryModel.getCategory(); 
      setCategory(category1)            
    };
    
    const onCategoryChange = async (e)=>{
      const categoryId1=e.target.value
      setCategoryId(categoryId1)
      let params={
        offset:offset,
        limit:limit,
        category_id:categoryId1,
        status:statusValue,
        search:search
      }  
      fetchProducts(params)
    }
    const onStatusChange = async (e)=>{
      const statusValue1=e.target.value
      setStatusValue(statusValue1)
      let params={
        offset:offset,
        limit:limit,
        category_id:categoryId,
        status:statusValue1,
        search:search
      }  
      fetchProducts(params)
    }
    const onSearch= async (e)=>{
    let searchValue=e.target.value
      setSearch(searchValue)
      let params={
        offset:offset,
        limit:limit,
        category_id:categoryId,
        status:statusValue,
        search:searchValue
      }  
      fetchProducts(params)
    }
    function updatePageNavigation(productId){
      navigate(`/updateproduct/${productId}`)
    }
   async function deleteProduct(product){
      const  result =await confirm(`Are you sure to delete product  ${product.name}`, confirmbuttons);
      if (result) {
          console.log("You click yes!");
         await dispatch.productModel.removeProduct(product.id);
         window.location.reload(false);
      }
    }
  
  return (
    <div>
       <button className="add">
         <Link to="/addproduct" style={{ textDecoration: 'none', color:'black' }}>+ Add</Link>
         </button>
      <div>
          <select className='category' onChange={onCategoryChange} > 
            <option value="">Select Category</option>           
            {category.map((item,index)=>
            <option key={index} value={item.id}>{item.name}</option>)}
          </select>       
      </div>
      <div >
          <select className='status' onChange={onStatusChange} > 
            <option value="">Select Status</option>           
            {status.map((item,index)=>
            <option key={index} value={item.value}>{item.value}</option>)}
          </select>       
      </div>
      <div>
          <input  type="text" className="search" onChange={onSearch} placeholder='Search Here' />
      </div>
     
      <Header/>
      <Sidebar/>
     
      <div className='tb1'>
   <h1>PRODUCTS</h1>

    <table border="4" className='tb2'>
      <thead > 
       <tr>
        <td className='td11'> ID</td>
        <td className='td11'>Name</td>
        <td className='td11'>Description</td>
        <td className='td11'>Price</td>
        <td className='td11'>Category</td>
        <td className='td11'>Status</td>
        <td className='td11'>Action</td>
       </tr> 
      </thead>
  
   <tbody>
{

name.map((item,index) =>
    <tr key={index}>
       <td className='td12'>{item.id}</td>
       <td className='td12'>{item.name}</td>
       <td className='td12'>{item.description}</td>
       <td className='td12'>{item.price}</td>
       <td className='td12'>{item.category.name}</td>
       <td className='td12'>{item.status}</td>
       <td className='td12'><button className='fa fa-pencil editicon' style={{'font-size':'15px'}} onClick={() =>{updatePageNavigation(item.id)}}></button><br/>
       <button className="fa fa-trash trashclr"  onClick={() => { deleteProduct(item) }}></button></td>
       
    </tr>
    )}
</tbody> 
</table>
    </div> 
    <ReactPaginate
 previousLabel={"<<"}
 nextLabel={">>"}
 breakLabel={"..."}
 pageCount={pageCount}
 marginPagesDisplayed={2}
 pageRangeDisplayed={3}
 onPageChange={handlePageClick}
 containerClassName={"pagination justify-content-center"}
 pageClassName={"page-item"}
 pageLinkClassName={"page-link"}
 previousClassName={"page-item"}
 previousLinkClassName={"page-link"}
 nextClassName={"page-item"}
 nextLinkClassName={"page-link"}
 breakClassName={"page-item"}
 breakLinkClassName={"page-link"}
 activeClassName={"active"}
/>
    </div>
  )
}


export default Product