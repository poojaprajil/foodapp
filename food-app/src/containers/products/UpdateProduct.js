import React, { useEffect, useState } from 'react';
import Header from '../../components/header/Header'
import Sidebar from '../../components/sidebar/Sidebar'
import { Formik, ErrorMessage } from 'formik';
import { useNavigate, useParams } from "react-router-dom";
import Select from 'react-select';
import { useDispatch } from 'react-redux';
import * as Yup from "yup";
import './UpdateProduct.css'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const UpdateProduct = () => {
  const [category, setCategoy] = useState([]);
  const [index, setIndex] = useState(0);
  const [statusIndex, setStatusIndex] = useState(0);
  const[image,setImage]=useState()
  let navigate = useNavigate();
  const dispatch = useDispatch();
  let [product, setProduct] = useState({});
  const { productId } = useParams();
   const  statusOption= [
    { value: 'active', label: 'active' },
    { value: 'inactive', label: 'inactive' },
    { value: 'trash', label: 'trash' }
  ];
  const notify = () => {
   
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }
  useEffect(() => {
    async function fetchCategory() {
      var categoryName =  await dispatch.categoryModel.getCategory();
      const options = [];
      categoryName.map((item) => {
        return (
          options.push({ value: item.id, label: item.name }))
      })
      setCategoy(options);
      let params = {
        id: productId
      }
      let products = await dispatch.productModel.getProduct(params); 
      setProduct(products.rows[0])
      setImage(products.rows[0].image)
      const category_id = options.findIndex(object => {
        return object.value === products.rows[0].category_id;
      });
       setIndex(category_id)
      const status = statusOption.findIndex(object => {
        return object.value === products.rows[0].status;
      });
       setStatusIndex(status)
    }
    fetchCategory();
  }, [])
  const handleSubmit = async value => {
    let payload = { id: productId, value: value }
    const res=  await dispatch.productModel.editProduct(payload);
    if(res.message==true){
    navigate('/product')
  }
  else{
   notify()
  }
 
  }
  return (
    <div>  <Header />
    <div > <Sidebar/></div> 
    <div className="updateproducts">
      <div className="adddiv"><h1>EDIT PRODUCT</h1></div>
        {/* <label>{product.category_id}</label> */}
        { product && product.id ? 
        <Formik
          initialValues={{
            name: product.name,
            description: product.description,
            status:product.status,
            category_id: product.category_id,
            price: product.price,
            image:product.image
          }}

          validationSchema={
            Yup.object().shape({
              name: Yup.string().required("Required"),
              description: Yup.string().required("Required"),
              status: Yup.string().required("Required"),
              category_id: Yup.string().required("Required"),
              price: Yup.string().required("Required")
            })
          }
          onSubmit={(values) => {
            handleSubmit(values)
          }}
        >
          {({ values,
           handleSubmit,
           handleChange,
           handleBlur,
           setFieldValue
             }) => (
              <form className="clearfix" method="POST" onSubmit={handleSubmit} noValidate>
              <ErrorMessage name="name"  component="span" className="form-error" />
              <input type="field" className="control1"
                placeholder="Name"
                name="name"
                id="name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
              />
              <br></br><br></br>
              <ErrorMessage name="description" component="span" className="form-error" />
              <textarea className="area" 
                placeholder="Description"
                name="description"
                id="description"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.description}
              />
              <br></br>
              <ErrorMessage name="status" component="span" className="form-error" />
              <Select className="select" 
                name="status"
                id="status"
                options={statusOption}             
                value={values.optionChange}
                defaultValue={statusOption[statusIndex]}
                onChange={value =>setFieldValue('status', value.value)} 
              />
              <br></br>
              <ErrorMessage name="category_id" component="span" className="form-error" />
              <Select className="control2" 
                name="category_id"
                id="category_id"
                placeholder="category id"            
                options={category}
                value={values.optionChange}
                defaultValue={category[index]}              
                onChange={value =>setFieldValue('category_id', value.value)} 
              />
              <br></br>
              <ErrorMessage name="price" component="span" className="form-error" />
              <br></br>
              <input type="text" className="control1"
                placeholder="price" 
                name="price"
                id="price"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.price}
              />
              <br></br>
              <label>Upload Image</label>
                    <input  type="file" className="box"
                      name="image"
                      id="image"
                      onChange={(e) => {
                        setFieldValue("image",e.target.files[0])
                      }}
                      onBlur={handleBlur}
                      
                    />
                    <br/><br/>
              <button type="submit" className="registerbtn">Edit</button>
              <input type="button" className="cancel" value="Cancel" onClick={() => navigate('/product')} />
            </form>
          )}
        </Formik>
:null}
      </div>
      
    </div>
  )
}



export default UpdateProduct