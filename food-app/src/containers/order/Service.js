import { api } from '../../helpers/Axios'

export function orderView(params) {
    let param = new URLSearchParams(params).toString();
    return api().get('/api/order/listorder?' + param)
        .then(res => {
            return res.data
        })
        .catch((e) => console.log("Error", e))
}

export function cartOrder(cart) {
    return api().post('/api/order/addtoorder', cart)
        .then((res) => {
            return res
        })
        .catch((e) => console.log("Error", e))
}



