import * as service from './Service';
import { toast } from 'react-toastify';

export default {
    state: {
        order :[]
    },
    reducers: {
        setOrderDetails: (state, data) => {
            
            return {
                ...state,...data
            };
        },
        displayError: (state, error) => {
            return toast(' 🚀' + error, {
                position: "top-right",
            });

           
        },
    },
    effects: {
        async getOrderData(params,state) {      
            try {
                let res = await service.orderView(params);
                this.setOrderDetails(res);
                if (res.message !== "true") {
                    this.displayError(res.message)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async orderCart(payload,state) {      
            try {
                let res = await service.cartOrder(payload);
                this.setOrderDetails(res);
                if (res.message !== "true") {
                    this.displayError(res.message)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        }
  
    }
};
