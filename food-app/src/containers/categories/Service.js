import {api} from '../../helpers/Axios'

export function categoryView(){    
  
   return api().get(`/api/categories/getcategories`)
   .then(res =>{    
       return res.data})
    .catch((e)=>console.log("Error",e))
        }