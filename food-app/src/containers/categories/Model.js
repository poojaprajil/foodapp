import * as service from './Service';


export default {
    state: {
        category :[]
    },
    reducers: {
        setCategoryDetails: (state, data) => {
            
            return {
                ...state,...data
            };
        },
    },
    effects: {
        async getCategory(params,state) {      
            try {
                let res = await service.categoryView(params);
                this.setCategoryDetails(res);
                return res;
            }
            catch (e) {
                console.log(e)
            }
        }
    }
};
