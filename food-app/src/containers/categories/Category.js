import React,{ useEffect, useState } from 'react'
import {  useDispatch } from 'react-redux';
import Header from '../../components/header/Header';
import Sidebar from '../../components/sidebar/Sidebar';



function Category() {
    const dispatch = useDispatch();
    const category=async()=>{
        const  data = await dispatch.categoryModel.getCategory(); 
    }
   category()
  return (
    <div>
        <Header/>
        <Sidebar/>
        <h1>Category</h1>
    </div>
  )
}

export default Category