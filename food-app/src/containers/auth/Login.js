import React from 'react'
import "./Login.css"
import { useNavigate } from 'react-router';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function Login() {

  const dispatch = useDispatch();
  let navigate = useNavigate();
  const notify = () => {
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }

  const formik = useFormik({
    initialValues: {

      email: '',
      password: ''
    },

    validate: values => {
      let errors = {}
      if (values.email.trim().length === 0) {
        errors.email = "email is required"
      }
      if (values.password.trim().length === 0) {
        errors.password = "password is required"
      }
      return errors
    },
    onSubmit: async (values) => {
      const data = await dispatch.authModel.signinUser(values)
      const role = data.role_name
      if (data) {
        if (role === "Customer") {
          navigate('/customer/home')
        }
        else if (role === "Admin") {
          navigate('/admin/user')
        }
      }
      else {
        notify();
      }
    },
  });


  return (
    <div className="login-page">

      <div className="form">

        <form className="login-form" method="POST" onSubmit={formik.handleSubmit} >
          <input type="text" placeholder="email" name='email' onChange={formik.handleChange} value={formik.values.email} />
          {formik.errors.email ? <div>{formik.errors.email}</div> : null}<br />
          <input type="password" placeholder="password" name='password' onChange={formik.handleChange} value={formik.values.password} />
          {formik.errors.password ? <div>{formik.errors.password}</div> : null}<br />
          <button type="submit" >login</button>

          <p className="message">Forgot Password? <a href="/forgotpassword">Reset Password</a></p>
        </form>
      </div>
    </div>
  )
}

export default Login