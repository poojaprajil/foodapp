import React from 'react'
import "./Login.css"
import { useNavigate } from 'react-router';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useParams } from 'react-router-dom';
import jwt from 'jwt-decode'

const ResetPassword = () => {
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const notify = () => {
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }
  const user = useParams();
  console.log(user.token)
  const decode = jwt(user.token)
  const formik = useFormik({
    initialValues: {
      password: '',
      confirm_password: ''
    },

    validate: values => {
      let errors = {}
      const passReg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
      if (values.password.trim().length === 0) {
        errors.password = "password is required"
      }
      else if (!passReg.test(values.password)) {
        errors.password = "password must contain minimum eight characters, at least one letter and one number"
      }
      if (values.confirm_password.trim().length === 0) {
        errors.confirm_password = "confirm password is required"
      }
      else if (values.password !== values.confirm_password) {
        errors.confirm_password = "Password didn't match"
      }
      return errors
    },
    onSubmit: async (values) => {
      let value = {
        token:user.token,
        email: decode.data,
        data:values
      }
      const data = await dispatch.authModel.resetPassword(value)
      notify();
      if(data.message=="successful Updation"){
        navigate('/signin')
      }

    },
  });
  return (
    <div className="login-page">
      <h1>Reset Password</h1>
      <div className="form">
        <form className="login-form" method="POST" onSubmit={formik.handleSubmit} >
          <input type="text" placeholder="Password" name='password' onChange={formik.handleChange} value={formik.values.password} />
          {formik.errors.password ? <div>{formik.errors.password}</div> : null}<br />
          <input type="password" placeholder="Confirm Password" name='confirm_password' onChange={formik.handleChange} value={formik.values.confirm_password} />
          {formik.errors.confirm_password ? <div>{formik.errors.confirm_password}</div> : null}<br />
          <button type="submit" >Submit</button>
        </form>
      </div>
    </div>
  )
}

export default ResetPassword