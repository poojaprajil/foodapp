import React from 'react'
import "./Login.css"
import { useNavigate } from 'react-router';
import {  useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
const ForgotPassword = () => {
    const dispatch = useDispatch();
    let navigate = useNavigate();
    const notify = () => {
        toast.error({
       position: toast.POSITION.TOP_RIGHT
     });
   }
 
  
    const formik = useFormik({
      initialValues: {
        email: ''
      },
     
     validate:values=>{
       let errors={}
         if( values.email.trim().length===0 ){
        errors.email = "email is required"
          }
           return errors
     },
     
      onSubmit:async (values) => {
       console.log(values)
       const value=values.email
      const data=  await dispatch.authModel.forgotPassword(value)
      console.log(data)
    //   const role=data.role_name
    //   if(data){
    //    if(role==="Customer"){ 
    //      navigate('/customer/home')
    //  }
    //  else if(role==="Admin"){
    //     navigate('/admin/user')
    //  }
    //   }
     
    //   else{
    //    notify();
    //  }                
      },
    });
       
 
   return (
     <div className="login-page">
         <h1>Reset Password</h1>
 
   <div className="form">
     <form className="login-form" method="POST" onSubmit={formik.handleSubmit} >
       <input type="text" placeholder="email" name='email' onChange={formik.handleChange}  value={formik.values.email} />
       { formik.errors.email ? <div>{ formik.errors.email }</div>:null}<br />
       <button type= "submit" >Submit</button>
     </form>
   </div>
 </div>
   )
}

export default ForgotPassword