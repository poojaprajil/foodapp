import { api } from '../../helpers/Axios'


export function userSignup(user) {
    return api().post('/api/user/signup', user)
        .then((res) => {
            return res.data
        })
        .catch((e) => console.log("Error", e))
}
export function userSignin(user) {
    return api().post('/api/user/signin', user)
        .then((res) => {
            return res.data
        })
        .catch((e) => console.log("Error", e))
}
export function resetPassword(email) {
    return api().post(`/api/user/forgotpassword?email=${email}`)
        .then((res) => {
            return res.data
        })
        .catch((e) => console.log("Error", e))
}
export function passwordUpdate(user) {
    return api().post(`/api/user/resetpassword?email=${user.email}&token=${user.token}`, user.data)
        .then((res) => {
            return res.data
        })
        .catch((e) => console.log("Error", e))
}


