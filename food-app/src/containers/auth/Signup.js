import "./Signup.css"
import React from 'react';

import { useNavigate } from 'react-router';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function Signup() {


  const dispatch = useDispatch();
  let navigate = useNavigate();
  const notify = () => {
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }


  const formik = useFormik({
    initialValues: {
      first_name: '',
      last_name: '',
      email: '',
      phone_number: '',
      password: ''
    },

    validate: values => {
      let errors = {}

      const passReg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/

      if (!(values.first_name)) {
        errors.first_name = "first name is required"
      }

      if (!(values.last_name)) {
        errors.last_name = "last name is required"
      }

      if (values.email.trim().length === 0) {
        errors.email = "email is required"
      }
      if (values.phone_number.trim().length === 0) {
        errors.phone_number = "phone number is required"
      }
      if (values.password.trim().length === 0) {
        errors.password = "password is required"
      }
      else if (values.phone_number.trim().length !== 10) {
        errors.phone_number = "Phone number must have 10 digits"
      }

      else if (!passReg.test(values.password)) {
        errors.password = "password must contain minimum eight characters, at least one letter and one number"
      }
      else if (values.password !== values.cpp) {
        errors.cpp = "Password didn't match"
      }
      return errors

    },

    onSubmit: async values => {

      let res = await dispatch.authModel.createUser(values)
      if (res.token) {
        navigate('/signin')
      } else {
        notify()
      }
    },
  });


  return (

    <div className="contain">
      <div className="container">
        <h1>Signup Form</h1>
        <form id="form1" method="POST" onSubmit={formik.handleSubmit} >
          <label >FirstName:</label>
          <input className="box" type="text" name="first_name" id="first_name" onChange={formik.handleChange} value={formik.values.first_name} /><br />
          {formik.errors.first_name ? <div>{formik.errors.first_name}</div> : null}<br />

          <label> LastName</label>
          <input className="box" type="text" name="last_name" id="last_name" onChange={formik.handleChange} value={formik.values.last_name} /><br />
          {formik.errors.last_name ? <div>{formik.errors.last_name}</div> : null}<br />


          <label>Email:</label>
          <input className="box" type="email" name="email" id="email" onChange={formik.handleChange} value={formik.values.email} /><br />
          {formik.errors.email ? <div>{formik.errors.email}</div> : null}<br />

          <label>PhoneNumber:</label>
          <input className="box" type="text" name="phone_number" id="phone_number" onChange={formik.handleChange} value={formik.values.phone_number} /><br />
          {formik.errors.phone_number ? <div>{formik.errors.phone_number}</div> : null}<br />


          <label>Password:</label>
          <input className="box" type="password" name="password" id="password" onChange={formik.handleChange} value={formik.values.password} /><br />
          {formik.errors.password ? <div>{formik.errors.password}</div> : null}<br />

          <label>Confirm Password:</label>
          <input className="box" type="password" name="cpp" id="cpp" onChange={formik.handleChange} value={formik.values.cpp} /><br />
          {formik.errors.cpp ? <div>{formik.errors.cpp}</div> : null}<br />


          <input type="submit" name="submit" value="Sign Up" id="submit" className="s" />
        </form>
      </div>
    </div>
  )
}



export default Signup