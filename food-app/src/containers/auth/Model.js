import * as service from './Service';
import { getCookie,saveCookies } from '../../helpers/Helpers';
import { toast } from 'react-toastify';


const auth= {
    state: {
        user : getCookie() ? getCookie() : {}
    },
    reducers: {
        setUserDetails: (state, data) => {
            saveCookies(data)
            return {
                ...state, user:data
            };
        },
        displayError: (state, error) => {
            return toast(' 🚀' + error, {
                position: "top-right",
            }); 
        },
    },
    effects: {
        async createUser(payload) {
            try {
                let res = await service.userSignup(payload);
                this.setUserDetails(res);
                if (res.message !== "true") {
                    this.displayError(res.message)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async signinUser(payload) {
            try {
                let res = await service.userSignin(payload);
                this.setUserDetails(res);
                if (res.message !== "true") {
                    this.displayError(res.message)
                    console.log(res)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async forgotPassword(payload) {
            try {
                let res = await service.resetPassword(payload);
                this.setUserDetails(res);
                if (res.message !== "true") {  
                    this.displayError(res.message)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async resetPassword(payload) {
            try {
                let res = await service.passwordUpdate(payload);
                this.setUserDetails(res);
                if (res.message !== "true") {
                   
                    this.displayError(res.message)
                    console.log(res)
                }
                return res;
            }
            catch (e) {
                console.log(e)
            }
        }
    }
};

export default auth;