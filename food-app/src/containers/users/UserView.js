import React, { useEffect, useState } from 'react';
import "./UserView.css"
import Header from '../../components/header/Header';
import Sidebar from '../../components/sidebar/Sidebar';
import ReactPaginate from 'react-paginate';
import { useDispatch } from 'react-redux';
import { confirm } from "react-confirm-box";
import { Link, useNavigate } from 'react-router-dom';

function UserView() {

  const [name, setName] = useState([])
  const [pageCount, setpageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(0)
  const [statusValue, setStatusValue] = useState("")
  const [roleName, setRoleName] = useState("")
  let params = {
    currentPage: 0, roleName: roleName, status: statusValue
  }
  let limit = 10;
  const dispatch = useDispatch();
  let navigate = useNavigate()
  const confirmbuttons = {
    labels: {
      confirmable: "Confirm",
      cancellable: "Cancel"
    }
  }


  useEffect(() => {
    async function listUser() {

      var data = await dispatch.userModel.viewUser(params);
      const total = data.count;
      setpageCount(Math.ceil(total / limit));
      setName(data.rows)

    }

    listUser()

  }, [limit])

  const fetchUsers = async (params) => {

    const data = await dispatch.userModel.viewUser(params);
    return (data.rows);
  };

  const handlePageClick = async (data) => {

    let currentP = data.selected + 1;
    let currentPage1 = (currentP - 1) * 10
    setCurrentPage(currentPage1)

    const total = data.count;
    let params = {
      currentPage: currentPage1, roleName: roleName, status: statusValue
    }
    const usersFormServer = await fetchUsers(params);
    setName(usersFormServer);
  };

  const handleChange = async (e) => {
    const roleName1 = e.target.value
    setRoleName(roleName)
    let params = {
      currentPage: 0, roleName: roleName1, status: statusValue
    }

    const names = await dispatch.userModel.viewUser(params);
    setName(names.rows)
  }
  const statusHandleChange = async (e) => {
    const status1 = e.target.value
    setStatusValue(status1)

    let params = {
      currentPage: 0, roleName: roleName, status: status1
    }

    const names = await dispatch.userModel.viewUser(params);
    setName(names.rows)
  }
  async function deleteUser(user) {
    const result = await confirm(`Are you sure to delete User  ${user.first_name}`, confirmbuttons);
    if (result) {
      console.log("You click yes!");
      await dispatch.userModel.removeUser(user.id);
    }
  }
  function updatePageNavigation(userId) {
    navigate(`/admin/updateuser/${userId}`)
  }


  return (

    <div>
      <div> <button className="add">
        <Link to="/admin/adduser" style={{ textDecoration: 'none', color: 'black' }}>+ Add</Link>
      </button>
      </div>
      <div>
        <select className='role' onChange={handleChange} >
          <option value="">Select Role</option>
          <option value="2">Admin</option>
          <option value="3">Super Admin</option>
          <option value="1">Customer</option>
        </select>
      </div>
      <div>
        <select className='status' onChange={statusHandleChange} >
          <option value="">select Status</option>
          <option value="active">Active</option>
          <option value="inactive">Inactive</option>
          <option value="trash">Trash</option>
        </select>
      </div>

      <Header />
      <Sidebar />



      <div className='tb1'>
        <h1>USERS</h1>
        <table border="4" className='tb3'>

          <thead >
            <tr>
              <th className='th2'> ID</th>
              <th className='th2'>First Name</th>
              <th className='th2'>Last Name</th>
              <th className='th2'>Email</th>
              <th className='th2'>Phone</th>
              <th className='th2'>Role</th>
              <th className='th2'>Action</th>

            </tr>
          </thead>

          <tbody>
            {

              name.map((item, index) =>
                <tr className='tr3' key={index}>
                  <td className='td2'>{item.id}</td>
                  <td className='td2'>{item.first_name}</td>
                  <td className='td2'>{item.last_name}</td>
                  <td className='td2'>{item.email}</td>
                  <td className='td2'>{item.phone_number}</td>
                  <td className='td2'>{item.role.role_name}</td>
                  <td className='td2'>
                    <button className='fa fa-pencil editicon' style={{'font-size':'15px'}} onClick={() => { updatePageNavigation(item.id) }}></button><br/>
                    <button className="fa fa-trash trashclr"  onClick={() => { deleteUser(item) }}></button></td>
                </tr>
              )}
          </tbody>
        </table>
      </div>


      <ReactPaginate
        previousLabel={"previous"}
        nextLabel={"next"}
        breakLabel={"..."}
        pageCount={pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={3}
        onPageChange={handlePageClick}
        containerClassName={"pagination justify-content-center"}
        pageClassName={"page-item"}
        pageLinkClassName={"page-link"}
        previousClassName={"page-item"}
        previousLinkClassName={"page-link"}
        nextClassName={"page-item"}
        nextLinkClassName={"page-link"}
        breakClassName={"page-item"}
        breakLinkClassName={"page-link"}
        activeClassName={"active"}
      />
    </div>
  )
}

export default UserView