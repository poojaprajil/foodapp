import React, { useState, useEffect } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import {
  Container,
  Grid,
  Typography
} from '@material-ui/core';

import Textfield from '../../components/meterialui/TextField';
import RoleSelect from '../../components/meterialui/CategorySelect';
import Button from '../../components/meterialui/Button';
import Header from '../../components/header/Header.js';
import Sidebar from '../../components/sidebar/Sidebar'
import { useNavigate } from 'react-router';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../users/AddUser.css'




const useStyles = makeStyles((theme) => ({
  formWrapper: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(8),
  },
}));

const INITIAL_FORM_STATE = {
  first_name: '',
  last_name: '',
  email: '',
  phone_number: '',
  password: '',
  role_id: ''
};



const FORM_VALIDATION = Yup.object().shape({
  first_name: Yup.string()
    .required('Required'),
  last_name: Yup.string()
    .required('Required'),
  email: Yup.string()
    .required('Required')
    .matches(
      /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
      "email format is not correct"
    ),
  phone_number: Yup.number()
    .integer()
    .typeError('Please enter a valid phone number')
    .required('Required'),
  password: Yup.string()
    .required('Required')
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
      "Password must contain at least 8 characters, one uppercase,one number"
    ),
  role_id: Yup.string()
    .required('Required'),
});
function AddUser() {
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const classes = useStyles();
  const [role, setRole] = useState([])
  const [selectedRole, setSelectedRole] = useState("");
  useEffect(() => {
    const fetchRole = async () => {
      const role1 = await dispatch.roleModel.getRoles();
      setRole(role1)
    };
    fetchRole()
  }, [])
  const notify = () => {
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }

  return (
    <div>
      <Header />
      <Sidebar />
      <div className='form1'>
        <Grid container>

          <Grid item xs={12}>
            <Container maxWidth="md">
              <div className={classes.formWrapper}>

                <Formik
                  initialValues={{
                    ...INITIAL_FORM_STATE
                  }}
                  validationSchema={FORM_VALIDATION}
                  onSubmit={async (values) => {
                    let res = await dispatch.authModel.createUser(values)
                    if (res.message === "Successfully added") {
                      navigate('/admin/user')
                    }
                    else {
                      notify()
                    }

                  }}
                >
                  <Form >

                    <Grid container spacing={2}>

                      <Grid item xs={12}>
                        <Typography>
                          <h1>ADD USER</h1>
                        </Typography>
                      </Grid>

                      <Grid item xs={6}>
                        <Textfield
                          name="first_name"
                          label="First Name"
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Textfield
                          name="last_name"
                          label="Last Name"
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Textfield
                          name="email"
                          label="Email"
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Textfield
                          name="phone_number"
                          label="Phone Number"
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <Textfield
                          name="password"
                          label="Password"
                        />
                      </Grid>



                      <Grid item xs={6}>

                        <RoleSelect
                          name="role_id"
                          label="Role"
                          options={role}
                        />
                      </Grid>

                      <Grid item xs={6}>
                        <Button>
                          Add
                        </Button>
                      </Grid>


                    </Grid>

                  </Form>
                </Formik>

              </div>
            </Container>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default AddUser