import React, { useEffect, useState } from 'react';
import Header from '../../components/header/Header'
import Sidebar from '../../components/sidebar/Sidebar'
import { Formik, ErrorMessage } from 'formik';
import { useNavigate, useParams } from "react-router-dom";
import Select from 'react-select';
import { useDispatch } from 'react-redux';
import * as Yup from "yup";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../users/UpdateUser.css'

const UpdateUser = () => {
  const [role, setRole] = useState([]);
  const [index, setIndex] = useState(0);
  const [statusIndex, setStatusIndex] = useState(0);
  const [newValues, setNewValues] = useState([])
  let navigate = useNavigate();
  const dispatch = useDispatch();
  let [user, setUser] = useState({});
  const { userId } = useParams();
  const statusOption = [
    { value: 'active', label: 'active' },
    { value: 'inactive', label: 'inactive' },
    { value: 'trash', label: 'trash' }
  ];
  const notify = () => {
    toast.error({
      position: toast.POSITION.TOP_RIGHT
    });
  }
  useEffect(() => {
    async function fetchRoles() {
      var roleNames = await dispatch.roleModel.getRoles();
      const options = [];
      roleNames.map((item) => {
        return (
          options.push({ value: item.id, label: item.role_name }))
      })
      setRole(options);
      let params = {
        id: userId
      }
      let users = await dispatch.userModel.viewUser(params);
      setUser(users.rows[0])
      const ind = options.findIndex(object => {
        return object.value == users.rows[0].role_id;
      });
      setIndex(ind)
      const status = statusOption.findIndex(object => {
        return object.value === users.rows[0].status;
      });
      setStatusIndex(status)
    }
    fetchRoles();
  }, [])
  const initialValues1 = {
    first_name: user.first_name,
    last_name: user.last_name,
    email: user.email,
    phone_number: user.phone_number,
    status: user.status,
    password: "",
    role_id: user.role_id,

  }
  const handleSubmit = async value => {
    let payload = { id: userId, value: value }
    const res = await dispatch.userModel.editUser(payload);
    if (res.message == "successful Updation") {
      navigate('/admin/user')
    }
    else {
      notify()
    }
  }
  return (
    <div>  <Header />
      <div > <Sidebar /></div>
      <div className="updateuser">
        <div className="adddiv"><h1>EDIT</h1></div>
        {/* <label>{user.role_id}</label> */}
        {user && user.id ?
          <Formik
            //  initialValues={initialValues1}

            validationSchema={
              Yup.object().shape({
                first_name: Yup.string()
                  .required('Required'),
                last_name: Yup.string()
                  .required('Required'),
                email: Yup.string()
                  .required('Required')
                  .matches(
                    /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                    "email format is not correct"
                  ),
                phone_number: Yup.number()
                  .integer()
                  .typeError('Please enter a valid phone number')
                  .required('Required'),
                // password: Yup.string()
                // .required('Required')
                // .matches(
                //   /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                //   "Password must contain at least 8 characters, one uppercase,one number"
                // ),
                role_id: Yup.string()
                  .required('Required'),
              })
            }
            initialValues={initialValues1}
            enableReinitialize={true}
            onSubmit={(values) => {
              const keys = Object.keys(initialValues1);
              let updatedValues = []
              for (let i = 0; i < keys.length; i++) {
                if (initialValues1[keys[i]] != values[keys[i]]) {
                  updatedValues.push({ [keys[i]]: values[keys[i]] })
                }
              }
              setNewValues(updatedValues)
              handleSubmit(updatedValues)
            }}
          >
            {({ values,
              handleSubmit,
              handleChange,
              handleBlur,
              setFieldValue
            }) => (
              <form className="clearfix" method="POST" onSubmit={handleSubmit} noValidate>
                <ErrorMessage name="first_name" component="span" className="form-error" />
                <input type="field" className="control1"
                  placeholder="First Name"
                  name="first_name"
                  id="firstname"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.first_name}
                />
                <br></br><br></br>
                <input type="field" className="control1"
                  placeholder="Last Name"
                  name="last_name"
                  id="lastname"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.last_name}
                />
                <br></br><br></br>
                <input type="field" className="control1"
                  placeholder="Email"
                  name="email"
                  id="email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.email}
                />
                <br></br><br></br>
                <input type="field" className="control1"
                  placeholder="Phone Number"
                  name="phone_number"
                  id="phonenumber"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone_number}
                />
                <br></br><br></br>
                <input type="field" className="control1"
                  placeholder="********"
                  name="password"
                  id="password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                />
                <br></br><br></br>
                <ErrorMessage name="status" component="span" className="form-error" />
                <Select className="select"
                  name="status"
                  id="status"
                  options={statusOption}
                  value={values.optionChange}
                  defaultValue={statusOption[statusIndex]}
                  onChange={value => setFieldValue('status', value.value)}
                />
                <br></br>
                <ErrorMessage name="role_id" component="span" className="form-error" />
                <Select className="control2"
                  name="role_id"
                  id="role_id"
                  placeholder="Role"
                  options={role}
                  value={values.optionChange}
                  defaultValue={role[index]}
                  onChange={value => setFieldValue('role_id', value.value)}
                />

                <br/>
                <button type="submit" className="registerbtn">Edit</button>
                <input type="button" className="cancel" value="Cancel" onClick={() => navigate('/product')} />
              </form>
            )}
          </Formik>
          : null}
      </div>

    </div>
  )
}



export default UpdateUser