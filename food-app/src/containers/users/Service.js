
import { api } from '../../helpers/Axios'

export function userView(params) {
  let param = new URLSearchParams(params).toString();
  return api().get('/api/user/userviews?' + param)
    .then(res => {
      return res.data
    }
    )
    .catch((e) => console.log("Error", e))
}

export async function deleteUser(id) {
  return api().delete(`/api/user/deleteuser?id=${id}`)
    .then((res) => res.data)
    .catch((e) => console.log("Error", e))
}

export function updateUser(user) {
  return api().put(`/api/user/updateuser?id=${user.id}`, user.value)
    .then((res) => res.data)
    .catch((e) => console.log("Error", e))
}
