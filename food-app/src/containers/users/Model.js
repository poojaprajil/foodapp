import * as service from './Service';


export default {
    state: {
        user: []
    },
    reducers: {
        setUserDetails: (state, data) => {
            return {
                ...state, ...data
            };
        },
    },
    effects: {
        async viewUser(params, state) {
            try {
                let res = await service.userView(params);
                this.setUserDetails(res);
                return res;

            }
            catch (e) {
                console.log(e)
            }
        },
        async removeUser(payload) {
            try {
                let res = await service.deleteUser(payload);
                this.setUserDetails(res);
                return res;
            }
            catch (e) {
                console.log(e)
            }
        },
        async editUser(payload) {
            try {
                let res = await service.updateUser(payload);
                this.setUserDetails(res);
                return res

            }
            catch (e) {
                console.log(e)
            }
        }

    }
};
