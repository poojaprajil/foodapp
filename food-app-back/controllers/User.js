const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const Sequelize = require("sequelize")
const userModel = require('../models/UserModel')
const generateAccessToken = require('../helpers/Helper');
const roleModel = require('../models/RoleModel');
const { Op } = Sequelize;
const TOKEN_SECRET = 'a5e87caeddd951eea1d197ebf6c4d9d1eb4bc1c47f491af640'
var nodemailer = require('nodemailer');
const e = require('express');
var transport = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    auth: {
        user: process.env.username,
        pass: process.env.password
    }
})

const UserSignup = async function (req, res) {
    try {
        let array = ["first_name", "last_name", "email", "phone_number", "password"]
        array.forEach(field => {
            if (!req.body[field]) {
                throw (`error:${field} is required`)
            }
        })
        // array.forEach((field) => {
        //     console.log(field,req.body[field])
        //     if ((!req.body[field]) && (req.body[field].trim() == "")) {
        //         console.log(field)
        //         throw (`error:${field} is required`)
        //     }
        // })

        var regex = /^[A-Za-z]+$/;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var phregx = /^[0-9-+]+$/;
        var passreg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (regex.test(req.body.first_name) == false) {
            throw ("error:name must contain only alphabets")
        }
        else if (emailReg.test(req.body.email) == false) {
            throw ("error:Email pattern not matching")
        }
        else if (regex.test(req.body.last_name) == false) {
            throw ("error:Last name must contain only alphabets ")
        }

        else if (phregx.test(req.body.phone_number) == false) {
            throw ("error:phone number must match pattern")
        }
        else if (passreg.test(req.body.password) == false) {
            throw ("error:password  must match Minimum eight characters, at least one letter and one number")
        }

        console.log(req.body)
        const username = req.body.email
        const phone = req.body.phone_number
        const data = await userModel.findOne({
            where: {
                [Op.or]: [{ email: username },
                { phone_number: phone }]
            }
        })
        if (data) {
            let user = { message: "User email or phone number alredy exist" }
            res.send(user)
            throw ("error:User email or phone number alredy exist")
        }

        else {
            const password_salt = Math.random().toString(36).slice(-8);
            const hash = crypto.createHmac('sha256', password_salt)
                .update(req.body.password)
                .digest('hex');

            let data1 = {
                first_name: req.body.first_name,
                //    last_name:req.body.last_name,
                email: req.body.email
            }
            const token = generateAccessToken({ data: data1 });
            let { first_name, last_name, email, phone_number, role_id } = req.body
            await userModel.create({
                first_name: first_name,
                last_name: last_name,
                email: email,
                phone_number: phone_number,
                password: hash,
                password_salt: password_salt,
                role_id: role_id

            }).then(function (userModel) {
                // var mailOptions={
                //     from:process.env.username,
                //     to:`${req.body.email}`,
                //     subject:'Login Crediantials',
                //     text:'Hello..your password is'+req.body.password
                // }
                // transport.sendMail(mailOptions,function(error,info){
                //     if(error){
                //         console.log(error)
                //     }
                //     else{
                //         console.log("Email send"+ info.response)
                //     }
                // })
                if (userModel) {
                    let include = [{
                        model: roleModel,
                        attributes: ['role_name'],
                        required: true,
                    }]
                    let user = {
                        id: userModel.id,
                        first_name: userModel.first_name,
                        last_name: userModel.last_name,
                        email: userModel.email,
                        status: userModel.status,
                        token: token,
                        role_id: userModel.role_id,
                        message: "Successfully added"
                    }
                    res.status(200).json(user)
                } else {
                    let user = { message: "Error in insert new record" }
                    res.send(user)
                    // throw ('Error in insert new record');
                }
            });
        }
    }
    catch (e) {
        res.status(400).json({
            status: "error",
            message: e
        })
    }

}


const userSigIn = async function (req, res) {
    try {
        const username = req.body.email
        const pass = req.body.password
        // console.log(username, pass, "hii")
        const data = await userModel.findOne({
            where: {
                email: username
            },
            include: [{
                model: roleModel,
                attributes: ['role_name'],
                required: true,
            }]

        });

        // console.log(data)
        if (data) {
            const secret = data.password_salt
            const password = data.password

            const hash = crypto.createHmac('sha256', secret)
                .update(pass)
                .digest('hex');
            if (hash == password) {
                //    jwt token generation
                const token = generateAccessToken({ data: data },'24h');
                // console.log(data.role.role_name, "----------")
                let user = {
                    id: data.id,
                    first_name: data.first_name,
                    last_name: data.last_name,
                    email: data.email,
                    status: data.status,
                    token: token,
                    role_id: data.role_id,
                    role_name: data.role.role_name,
                    message: true
                }
                // console.log(user)
                res.send(user)
            }
            else {
                let user = { message: "Wrong password" }
                res.send(user)
                // throw ({ code: 400, message: "Requested user not present" });
            }
        }
        else {
            let user = { message: "Authentication failed " }
            res.send(user)
        }
    }
    catch (e) {
        res.status(400).json({
            status: "error",
            message: e
        })
    }
}

const userViews = async function (req, res) {
    let condition = {}
    let conditions = {}
    let role_name = parseInt(req.query.roleName)
    if (req.query.currentPage) {
        offset = parseInt(req.query.currentPage)
    }
    else {
        offset = 0
    }
    if (role_name) {
        conditions = { role_id: role_name }
    }
    if (req.query.status) {
        conditions["status"] = req.query.status

    }
    else {
        conditions["status"] = { [Op.ne]: "trash" };
    }
    if (req.query.id) {
        conditions["id"] = parseInt(req.query.id);
    }
    //   console.log(conditions)
    let include = [
        {
            model: roleModel,
            attributes: ['role_name'],
            required: true,
        }
    ]
    var attributes = ['id', 'first_name', 'last_name', 'email', 'phone_number', 'status', 'role_id']
    condition = {
        where: conditions,
        attributes,
        offset: offset,
        limit: 5,
        include
    }
    try {
        await userModel.findAndCountAll(condition).then((data, err) => {
            if (err) console.log(err)
            else res.status(200).json(data)
        });
    }
    catch (e) {
        res.send(e)
    }
}
const deleteUser = async function (req, res) {
    // console.log(req.query.id)
    await userModel.findOne({
        attributes: ['id', 'status'],
        where: { id: req.query.id }
    })
        .then((user) => {
            if (user) {

                userModel.update({ status: "trash" }, { where: { id: req.query.id } })
                    .then((deleteUser) => {

                        return res.status(202).json({
                            "message": "User deleted successfully",
                            deleteUser
                        })
                    })

            } else {
                return res.status(206).json({
                    "message": "User not found"
                })
            }
        }).catch(error => {
            return res.status(400).json({
                "error": error
            })
        })

}
const updateUser = async function (req, res) {
    try {
        console.log(req.body, "update")
        var updateValues = Object.assign({}, ...req.body);
        // console.log(updateValues)
        const id = req.query.id;
        console.log(id)
        if (updateValues.password) {
            // console.log("oldpassword",updateValues.password)
            const password_salt = Math.random().toString(36).slice(-8);
            const hash = crypto.createHmac('sha256', password_salt)
                .update(updateValues.password)
                .digest('hex');
            // console.log(hash,updateValues)
            updateValues.password = hash
            Object.assign(updateValues, { password_salt: password_salt })

            // console.log(updateValues)
        }
        if (updateValues.email || updateValues.phone_number) {
            console.log("email")
            const email1 = updateValues.email
            const phone = updateValues.phone_number
            // console.log(updateValues)
            const datas = await userModel.findOne({
                where: {
                    [Op.or]: [{ email: email1 },
                    { phone_number: phone }]
                }
            })
            console.log(datas)
            if (datas) {
                // console.log("else")
                let user = { message: "User email or phone number alredy exist" }
                res.send(user)
                throw ("error:User email or phone number alredy exist")

            }
        }

        await userModel.update(updateValues, { where: { id: id } }).then(data => {
            if (data == 1) {
                let response = { message: "successful Updation" }
                res.status(200).send(response);

            }
            else { throw ("error:updation failed") }
        })
    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
const forgotPassword = async function (req, res) {
    try {
        const data = await userModel.findOne({
            where: {
                email: req.query.email
            }
        })
        if (data) {
            const value = req.query.email
            const recovery_token = generateAccessToken({ data: value },'2m');
            var mailOptions = {
                from: process.env.username,
                to: `${req.query.email}`,
                subject: 'Reset Password',
                text: 'click below link to reset password',
                html: '<p>Click <a href="http://localhost:3000/resetpassword/' + recovery_token + '">here</a>to reset your password</p>'
            }
            transport.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error)
                }
                else {
                    console.log("Email send" + info.response)
                }
            })
            res.status(200).send({ message: "Check Email to Reset Password " });
        }
        else {
            return res.status(206).json({
                "message": "User not found"
            })
        }

    }
    catch {
        res.status(400).json({
            status: "error",
            message: e
        })
    }

}
const resetPassword = async function (req, res) {
    try {
        console.log(req.query.token,req.query.email,"back req")
        let token=req.query.token
        let flag
        console.log(TOKEN_SECRET,"jwt")
        jwt.verify(token, TOKEN_SECRET, function (err, decoded) {
                   console.log("verify",err,decoded)
            if (err) {
                flag=0
                    res.status(200).json({
                        message: "inavalid token or expired token"
                    })
            }
            else{
                flag=1
            }
        })
    if(flag==1){
        const data = await userModel.findOne({
            where: {
                email: req.query.email
            }
        })
        if (data) {
            const password_salt = Math.random().toString(36).slice(-8);
            const hash = crypto.createHmac('sha256', password_salt)
                .update(req.body.password)
                .digest('hex');
            // console.log(hash,updateValues)
            updateValues = {
                password: hash,
                password_salt: password_salt
            }
            let data = await userModel.update(updateValues, { where: { email: req.query.email } })
            if (data == 1) {
                let response = { message: "successful Updation" }
                res.status(200).send(response);

            }
            else { throw ("error:updation failed") }

        }
        else {
            return res.status(206).json({
                "message": "User not found"
            })
        }
     
    }
       

    }
    catch {
        res.status(400).json({
            status: "error",
            message: e
        })
    }

}

module.exports = { UserSignup, userSigIn, userViews, deleteUser, updateUser, forgotPassword, resetPassword }