const Sequelize = require("sequelize")
const categoriesModel=require('../models/CategoriesModel')





const addCategories = async function (req, res) {
    try {
        let array = ["name", "description"]
        array.forEach((field) => {           
            if ((!req.body[field])) {
                throw (`error:${field} is required`)
            }
        })
       
        await categoriesModel.create({
            name: req.body.name,
            description: req.body.description,                                   
            message:"true"

        }).then((categories) => {
            if (categories) {
                res.send(categories)
            }
            else {
                throw ({ code: 400, message: "error in adding category" });
            }
        })
    }
    catch (e) {
        res.send(e);
        res.end()
    }
}

const getCategories= async function (req, res) {
    categoriesModel.findAll({
        attributes: ['id', 'name', 'description'],
       
    }) .then((categories) => {
        return res.status(200).json(categories)
    }).catch(err => {
        return res.status(400).json({ err })
    })

}

module.exports = {getCategories,addCategories}