const Sequelize = require("sequelize")
const roleModel=require('../models/RoleModel')

const getRoles= async function (req, res) {
    roleModel.findAll({
        attributes: ['id','role_name'],
       
    }) .then((roles) => {
        return res.status(200).json(roles)
    }).catch(err => {
        return res.status(400).json({ err })
    })

}

module.exports = getRoles