const Sequelize = require("sequelize")
const { Op } = require('sequelize');
const cartModel=require('../models/CartModel')
const productModel=require('../models/ProductModel')


const addToCart = async function (req, res) {
    try {
        console.log(req.body)
        let array = ["product_id", "product_name", "user_id"]
        array.forEach((field) => {           
            if ((!req.body[field])) {
                throw (`error:${field} is required`)
            }
        })
        // console.log(req.body.count)
        if(req.body.count==0){
            let cartCount = await cartModel.findOne({
             attributes: ['id', 'product_id', 'count','user_id'],
             where:{
                 [Op.and]: [{ product_id: req.body.product_id},
                     {user_id:req.body.user_id}
                 ]}
         });
         if(cartCount){
             let condition={user_id:req.body.user_id,
                            product_id:req.body.product_id}
                          await  cartModel.destroy({ where: condition})
         }

        }
        if(req.body.count >0){
            // console.log(req.body.count,"reqst")
           let counter=req.body.count
           let cartCount = await cartModel.findOne({
            attributes: ['id', 'product_id', 'count','user_id'],
            where:{
                [Op.and]: [{ product_id: req.body.product_id},
                    {user_id:req.body.user_id}
                ]}
        });
        // console.log(cartCount)
        if(cartCount){
           await cartModel.update({ count:counter }, {
                where:{
                    [Op.and]: [{ product_id: req.body.product_id},
                        {user_id:req.body.user_id}
                       
                    ]
                 }
                
                 }) .then((product) => {
                    console.log(product)
                    return res.status(200).json({"message":"updated cart successfully",product})
                })

        }
        else{
            // console.log("elase")
          await  cartModel.create({
                product_id: req.body.product_id,
                product_name: req.body.product_name,
                count:parseInt(req.body.count) ,
                user_id: req.body.user_id
            }).then((cartModel) => {
                if (cartModel) {
                    cartModel={cartModel,message:"cart added successfully"}
                    res.send(cartModel)
                    console.log(cartModel)
                }
                else {
                    res.send({message: "error in adding into cart" });
                }})

        }
    
 }
 else{
    res.send({message:"Please select a valied count"});
 }
}
       
    catch (e) {
        console.log(e)
        res.send(e);
        res.end()
    }
}
const listCart = async function (req, res) {
    try {
        // console.log("hi")
        var condition = {};
        if (req.query.user_id) {
            condition["user_id"] = parseInt(req.query.user_id);
        }
        
        if (req.query.product_id) {
            condition["product_id"] = parseInt(req.query.product_id);
        }
        // console.log(condition)
        await cartModel.findAndCountAll({where:condition,
            attributes:['user_id','product_id','product_name','count'],
            include: [
                {
                    model: productModel,
                    attributes: ['price','image']        
               }
            ]
        }).then(data => {
            if (data.count > 0) {
                res.send(data) }
            else {  res.send({message:"no product found"}) }
        })
   }
catch (e) {
    res.send(e)
 }
}
const deleteCartItem= async function (req, res) {
    let condition={}
    if (req.query.user_id) {
        condition["user_id"] = parseInt(req.query.user_id);
    }
    
    if (req.query.product_id) {
        condition["product_id"] = parseInt(req.query.product_id);
    }
    // console.log(condition)
    await cartModel.findOne({
        where:condition
    })
        .then((cart) => {
            if (cart) {

                cartModel.destroy({ where: condition})
                    .then((deleteCart) => {

                        return res.status(202).json({
                            "message": "cart Item deleted successfully",
                            deleteCart
                        })
                    })

            } else {
                return res.status(206).json({
                    "message": "cart not found"
                })
            }
        }).catch(error => {
            return res.status(400).json({
                "error": error
            })
        })
       
}
const updateCart = async function (req, res) {
    try {
        console.log(req.body.value)
        let condition={}
        if (req.body.user_id) {
            condition["user_id"] = parseInt(req.body.user_id);
        }
        
        if (req.body.product_id) {
            condition["product_id"] = parseInt(req.body.product_id);
        }
        console.log(condition)
        await cartModel.update(req.body, { where:condition}).then(data => {
            if (data == 1) {
                let response={message:true}
                res.status(200).send(response);

            }
            else { throw ("error:updation failed") }
        })
    }

    catch (e) {


        res.status(400).json({
            status: "error",
            message: e
        })
    }
}

module.exports={addToCart,listCart,deleteCartItem,updateCart}