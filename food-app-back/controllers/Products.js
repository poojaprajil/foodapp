const Sequelize = require("sequelize")
const { Op } = require('sequelize');
const productModel=require('../models/ProductModel')
const categoriesModel= require ("../models/CategoriesModel")


const addProduct = async function (req, res) {
    try {
        
        let array = ["name", "description", "category_id", "price"]
        array.forEach((field) => {           
            if ((!req.body[field])) {
                throw (`error:${field} is required`)
            }
        })
        console.log(req.file,req.body,"body")
        req.body.image=req.file.path
        // console.log(req.file)
        await productModel.create(
           { name: req.body.name,
            description: req.body.description,
            status: "active",
            price: req.body.price,
            image:req.file.path,
            category_id: req.body.category_id}
        )
        .then((productModel) => {
            if (productModel) {
                console.log(productModel)
                productModel={productModel,message:true}
                res.send(productModel)
            }
            else {
                throw ({ code: 400, message: "error in adding products" });
            }
        })
    }
    catch (e) {
        res.send(e);
        res.end()
    }
}

const listProducts= async function (req, res) {
    let condition={}
    // console.log(req.query.category_id)
    if(req.query.category_id && req.query.category_id !==""){
        condition["category_id"]=parseInt(req.query.category_id)
    }
    
    if(req.query.offset){
        offset=parseInt(req.query.offset)
           }
           else{
               offset=0
           }
    if(req.query.limit){
        limit=parseInt(req.query.limit)
    }
    else{
        limit=5
    }
    if(req.query.status){
        condition["status"]=req.query.status
    }
    else{
       condition["status"]= { [Op.ne]:"trash"} ;
    }
    if(req.query.id)
    {
        condition["id"]=parseInt(req.query.id);
    }

    if (req.query.search) {
        let searchQuery = req.query.search;
        // console.log(searchQuery)
        condition[Op.or] = 
             [
                { name: { [Op.like]: `%${searchQuery}%` } },
                { description: { [Op.like]: `%${searchQuery}%` } },
            ]
    }
    productModel.findAndCountAll({where:condition,
        attributes:['id','name','description','status','price','image','category_id'],
        include: [
            {
                 model: categoriesModel,
                 attributes: ['name'],
                 required: true,          
           }
        ],
        offset:offset,
        limit:limit
       
    }) .then((product) => {
        return res.status(200).json(product)
    }).catch(err => {
        return res.status(400).json({ err })
    })

}

const updateProduct = async function (req, res) {
    try {
    //    console.log(req.body,"file",req.file,"req")
        const id = req.query.id;
      if(req.file){
        req.body.image=req.file.path
      }
        await productModel.update(req.body, { where: { id: id } }).then(data => {
            if (data == 1) {
                let response={message:true}
                res.status(200).send(response);
            }
            else { throw ("error:updation failed") }
        })
    }

    catch (e) {
        res.status(400).json({
            status: "error",
            message: e
        })
    }
}
const deleteProduct = async function (req, res) {
    // console.log(req.query.id)
    await productModel.findOne({
        attributes: ['id', 'status'],
        where: { id: req.query.id }
    })
        .then((product) => {
            if (product) {
                productModel.update({status:"trash"}, { where: { id: req.query.id } })
                    .then((deleteProduct) => {
                        return res.status(202).json({
                            "message": "Product deleted successfully",
                            deleteProduct
                        })
                    })

            } else {
                return res.status(206).json({
                    "message": "Product not found"
                })
            }
        }).catch(error => {
            return res.status(400).json({
                "error": error
            })
        })



}



module.exports = {addProduct,listProducts,updateProduct,deleteProduct}