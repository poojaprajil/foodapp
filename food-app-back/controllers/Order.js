const Sequelize = require("sequelize")
const { Op } = require('sequelize');
const cartModel = require("../models/CartModel");
const orderModel=require('../models/OrderModel')
const productModel=require('../models/ProductModel')
var nodemailer=require('nodemailer')
var transport=nodemailer.createTransport({
    service:'gmail',
    host: 'smtp.gmail.com',
    auth: {
        user: process.env.username,
        pass: process.env.password
    }
})

const addToOrder = async function (req, res) {
    try {
        const {body}=req
        console.log(body)
        await orderModel.bulkCreate(body).then(async (orderModel) => {
            if  (orderModel) {
                 // var mailOptions={
                //     from:process.env.username,
                //     to:`${req.body.email}`,
                //     subject:'Login Crediantials',
                //     text:'Your order placed with reference id'+req.orderModel.reference_id
                // }
                // transport.sendMail(mailOptions,function(error,info){
                //     if(error){
                //         console.log(error)
                //     }
                //     else{
                //         console.log("Email send"+ info.response)
                //     }
                // })
                console.log(orderModel)
               await cartModel.destroy({
                    where: {
                      user_id: body[0].user_id
                    }
                  });
        
                res.send({orderModel,message:true})
            }
            else {
                throw ({ code: 400, message: "error ordering" });
            }
        })
    }
    catch (e) {
        res.send(e);
        res.end()
    }
}
const listOrder = async function (req, res) {
    try {
        
        var condition = {};
        if (req.query.user_id) {
            condition["user_id"] = parseInt(req.query.user_id);
        }
        // console.log(condition)
        await orderModel.findAndCountAll({where:condition,
            include: [
                {
                    model: productModel,
                    attributes: ['price','name','image']        
               }
            ]
        }).then(data => {
            if (data) {
                res.send(data) 
            }
            else {  res.send({message:"no order found"}) }
        })
   }
catch (e) {
    res.send(e)
 }
}
module.exports = {addToOrder,listOrder}
