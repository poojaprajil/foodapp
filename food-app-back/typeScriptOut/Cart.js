"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cartModel = require('../models/CartModel');
const productModel = require('../models/ProductModel');
const listCart = function (req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            var condition = {};
            var query = req.query;
            if (query.user_id) {
                condition["user_id"] = parseInt(query.user_id);
            }
            if (query.product_id) {
                condition["product_id"] = parseInt(query.product_id);
            }
            yield cartModel.findAndCountAll({ where: condition,
                attributes: ['user_id', 'product_id', 'product_name', 'count'],
                include: [
                    {
                        model: productModel,
                        attributes: ['price', 'image']
                    }
                ]
            }).then((data) => {
                if (data.count > 0) {
                    res.send(data);
                }
                else {
                    res.send({ message: "no product found" });
                }
            });
        }
        catch (e) {
            res.send(e);
        }
    });
};
module.exports = listCart;
//# sourceMappingURL=Cart.js.map