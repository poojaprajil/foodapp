const sequelize = require('../db/Connection').sequelize
const Sequelize  = require("sequelize");
const roleModel=require('./RoleModel')



let userModel = sequelize.define(
    "users",
    {
        id: {
            type: Sequelize.INTEGER(30),
            primaryKey: true,
            autoIncrement: true

        },
        first_name: {
            type: Sequelize.STRING(45),
            allowNull: false

        },

        last_name: {
            type: Sequelize.STRING(45),
            allowNull: false
        },
        email:{
            type: Sequelize.STRING(40),
            allowNull: false,
            unique: true
        },
        phone_number:{
            type: Sequelize.STRING(10),
            allowNull: false, 
            unique: true
        },
        password:{
            type: Sequelize.STRING(500),
            allowNull: false
        },
        password_salt:{
            type: Sequelize.STRING(500),
            allowNull: false
        },
        status: {
            type: Sequelize.ENUM("active", "inactive", "trash"),
            defaultValue: "active",
            allowNull: false
        },
        role_id:{
            type: Sequelize.INTEGER(10),
            defaultValue: 1,
            references:{
                model:'roles',
                key:"id"
            }
        }
        
       

    },
    {
        freezeTableName: true, timestamps: true,
        indexes: [
            {
                name: 'first_name',
                fields: ['first_name']
            },
        ]
    }
   )
   userModel.sync()
   roleModel.hasMany(userModel,{foreignKey:'role_id'})
   userModel.belongsTo(roleModel,{foreignKey:'role_id'})

module.exports=userModel