const sequelize = require('../db/Connection').sequelize
const Sequelize  = require("sequelize");


let categoriesModel= sequelize.define(
    'categories', 
    {
    id: {
        type: Sequelize.INTEGER(10),
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING(45),
        allowNull: false
    },
    description: {
        type: Sequelize.STRING(45),
        allowNull: false
    },
  

   

},
    {
        freezeTableName: true, timestamps: false,
        indexes: [
            {
                name: 'name',
                fields: ['name']
            },
        ]
    }
);

categoriesModel.sync()

module.exports=categoriesModel