const sequelize = require('../db/Connection').sequelize
const Sequelize  = require("sequelize");
let categoriesModel=require('../models/CategoriesModel')

const productModel= sequelize.define(
    'products',
     {
        id: {
            type: Sequelize.INTEGER(10),
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(45),
            allowNull: false
        },
        description: {
            type: Sequelize.STRING(100),
            allowNull: false
        },
        status: {
            type: Sequelize.ENUM("active", "inactive", "trash"),
            defaultValue: "active",
            allowNull: false
        },
        price: {
            type: Sequelize.INTEGER(10),
            allowNull: false
         },
         image:{
             type:Sequelize.STRING
         },
      
        category_id: {
           
            type: Sequelize.INTEGER(10),
            allowNull: false,
            references: {
                model: 'categories',
                key: 'id' 
            }
    
        },
       
            
        
    
    },
        {
            freezeTableName: true, timestamps: true,
            indexes: [
                {
                    name: 'name',
                    fields: ['name']
                },
            ]
            
        }
    );
    categoriesModel.hasMany(productModel,{foreignKey:'category_id'});
    productModel.belongsTo(categoriesModel,{foreignKey:'category_id'})
    productModel.sync()

    module.exports=productModel
    