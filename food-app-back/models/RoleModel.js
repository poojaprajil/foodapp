const sequelize = require('../db/Connection').sequelize
const Sequelize  = require("sequelize");


let roleModel = sequelize.define(
    "roles",
    {
        id: {
            type: Sequelize.INTEGER(10),
            primaryKey: true,
            autoIncrement: true

        },
        
        role_name:{
            type: Sequelize.STRING(45),
            allowNull: false
        }
       

    },
    {
        freezeTableName: true, timestamps: true,
        indexes: [
            {
                name: 'role_name',
                fields: ['role_name']
            },
        ]
    }
)
roleModel.sync()

module.exports=roleModel