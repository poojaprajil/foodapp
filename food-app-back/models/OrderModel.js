const sequelize = require('../db/Connection').sequelize
const Sequelize  = require("sequelize");
let productModel=require('../models/ProductModel')
let userModel=require('../models/UserModel')

const orderModel= sequelize.define(
    'orders',
     {
        id: {
            type: Sequelize.INTEGER(10),
            primaryKey: true,
            autoIncrement: true
        },
        reference_id:{
            allowNull:false,
            type:Sequelize.STRING(45)
          },
          user_id: {
            type: Sequelize.INTEGER(10),
            references: {
                model: 'users',
                key: 'id' 
            }
         },
        
        product_id: {
            type: Sequelize.INTEGER(10),
            references: {
                model: 'products',
                key: 'id' 
            }
        },
        item_count: {
            type: Sequelize.INTEGER,
            allowNull: false,
          },
          amount: {
            type: Sequelize.INTEGER,
            allowNull: false,
          }
    },
        {
            freezeTableName: true, timestamps: true
        }
    );
    productModel.hasMany(orderModel, { foreignKey: 'product_id' });
    orderModel.belongsTo(productModel, { foreignKey: 'product_id' })
    userModel.hasMany(orderModel, { foreignKey: 'user_id' });
    orderModel.belongsTo(userModel, { foreignKey: 'user_id' })
    orderModel.sync()

    module.exports=orderModel
    