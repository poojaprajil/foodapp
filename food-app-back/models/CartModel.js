const sequelize = require('../db/Connection').sequelize
const Sequelize  = require("sequelize");
let productModel=require('../models/ProductModel')
let userModel=require('../models/UserModel')

const cartModel= sequelize.define(
    'cart',
     {
        id: {
            type: Sequelize.INTEGER(10),
            primaryKey: true,
            autoIncrement: true
        },
        product_id: {
            type: Sequelize.INTEGER(10),
            references: {
                model: 'products',
                key: 'id' 
            }
        },
        product_name: {
            type: Sequelize.STRING(40),
        },
        count: {
            type: Sequelize.INTEGER(10),
        },
        user_id: {
            type: Sequelize.INTEGER(10),
            references: {
                model: 'users',
                key: 'id' 
            }
         }    
    },
        {
            freezeTableName: true, timestamps: true
        }
    );
    productModel.hasMany(cartModel, { foreignKey: 'product_id' });
    cartModel.belongsTo(productModel, { foreignKey: 'product_id' })
    userModel.hasOne(cartModel, { foreignKey: 'user_id' });
    cartModel.belongsTo(userModel, { foreignKey: 'user_id' })
    cartModel.sync()

    module.exports=cartModel
    