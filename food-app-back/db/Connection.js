const Sequelize = require("sequelize") ;
require("dotenv").config();
let sequelize = new Sequelize(
  'food_app',
  'root',
  'password',
  {
    dialect: "mysql",
    operatorsAliases: 0,
    logging: false,

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    replication: {
      read: [
        {
          host: 'localhost',
          username: 'root',
          password: 'password',
        },
      ],
      write: {
        host: 'localhost',
        username: 'root',
        password: 'password',
      },
    },
  }
);

sequelize
        .authenticate()
        .then(() => {
            console.log('Connection has been established successfully.');
        })
        .catch((err) => {
            console.log('Unable to connect to the database:', err);
        });

module.exports.sequelize=sequelize
