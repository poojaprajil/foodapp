let chai =require('chai')
let chaiHttp=require('chai-http')
const { response } = require('express')
let server=require('../app')

chai.should()

chai.use(chaiHttp)

describe('ORDER API',()=>{
    describe('GET/api/order',()=>{
        it("list Order",(done)=>{
            chai.request(server)
            .get("/api/order/listorder?user_id=111")
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("object")
                response.body.rows[0].should.have.property("id")
                response.body.rows[0].should.have.property("reference_id")
                response.body.rows[0].should.have.property("user_id")
                response.body.rows[0].should.have.property("product_id")
                response.body.rows[0].should.have.property("item_count")
                response.body.rows[0].should.have.property("amount")
                done()
            })
        })
    })

    describe('POST/api/order',()=>{
        it("order cart item",(done)=>{
            const cartItem=[{
                reference_id:"f2ovs7bi",
                user_id:111,
                product_id:7,
                item_count:1,
                amount:200          
            }]
            chai.request(server)
            .post("/api/order/addtoorder")
            .send(cartItem)
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("object")
                done()
            })
        })
    })


})