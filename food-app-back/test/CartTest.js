let chai =require('chai')
let chaiHttp=require('chai-http')
const { response } = require('express')
let server=require('../app')

chai.should()

chai.use(chaiHttp)

describe('CART API',()=>{
    describe('GET/api/cart',()=>{
        it("list cart",(done)=>{
            chai.request(server)
            .get("/api/cart/listCartItems?user_id=91")
            .end((err,response)=>{
                // console.log(response.body.rows)
                response.should.have.status(200)
                response.body.should.be.a("object")
                response.body.rows[0].should.have.property("user_id")
                response.body.rows[0].should.have.property("product_id")
                response.body.rows[0].should.have.property("product_name")
                response.body.rows[0].should.have.property("count")
                response.body.rows[0].product.should.have.property("price")
                response.body.rows[0].product.should.have.property("image")
                done()
            })
        })
    })

    describe('POST/api/cart',()=>{
        it("add to cart",(done)=>{
            const cart={
                product_id:8,
                product_name:"gfd",
                count:1,
                user_id:91            
            }
            chai.request(server)
            .post("/api/cart/addtocart")
            .send(cart)
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("object")
                response.body.should.have.property("message")
                done()
            })
        })
    })

    describe('DELETE/api/cart',()=>{
        it("delete cart item",(done)=>{
             chai.request(server)
            .delete('/api/cart/deletecartitem?user_id=111&product_id=3')
            .end((err,response)=>{
                response.should.have.status(202)
                response.body.should.have.property("message")
                done()
            })
        })
    })
    describe('PUT/api/cart',()=>{
        it("cart update",(done)=>{
            const cartData = {
                product_id:4,
                product_name:"Ghee Rice",
                count:2,
                user_id:111
            }
            chai.request(server)
            .put('/api/cart/updatecartitem')
            .send(cartData)
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.have.property("message")
                done()
            })
        })
    })
    

})