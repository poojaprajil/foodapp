let chai =require('chai')
let chaiHttp=require('chai-http')
const { response } = require('express')
let server=require('../app')

chai.should()

chai.use(chaiHttp)

describe('PRODUCT API',()=>{
    describe('GET/api/product',()=>{
        it("get all products",(done)=>{
            chai.request(server)
            .get("/api/product/listproducts")
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("object")
                response.body.rows[0].should.have.property("id")
                response.body.rows[0].should.have.property("name")
                response.body.rows[0].should.have.property("description")
                response.body.rows[0].should.have.property("status")
                response.body.rows[0].should.have.property("price")
                response.body.rows[0].should.have.property("image")
                response.body.rows[0].should.have.property("category_id")
                response.body.rows[0].category.should.have.property("name")
                done()
            })
        })
    })

    describe('POST/api/product',()=>{
        it("add product",(done)=>{
            chai.request(server)
            .post("/api/product/addproduct")
            .attach('image','/home/toobler/Desktop/projects/foodappnew/foodapp/shri-Q4CX55VsBP0-unsplash.jpg')
            .field('name', 'Dosa')
            .field('description','ghee rice dosa')            
            .field('category_id',2)
            .field('price',50)
            .end((err,response)=>{
                console.log(response)
                response.should.have.status(200)
                response.body.should.be.a("object")
                done()
            })
        })
    })
    describe('DELETE/api/product',()=>{
        it("delete product by id",(done)=>{
            // console.log(id)
            chai.request(server)
            .delete('/api/product/deleteproduct?id=8')
            .end((err,response)=>{
                response.should.have.status(202)
                response.body.should.have.property("message")
                done()
            })
        })
    })
    describe('PUT/api/product',()=>{
        it("user update",(done)=>{
            chai.request(server)
            .put('/api/product/updateproduct?id=12')
            .attach('image','/home/toobler/Desktop/projects/foodappnew/foodapp/63841432.jpg')
            .field('description','soft ghee rice dosa')            
            .field('category_id',1)
            .field('price',55)
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.have.property("message")
                done()
            })
        })
    })
    

})