let chai =require('chai')
let chaiHttp=require('chai-http')
const { response } = require('express')
let server=require('../app')

chai.should()

chai.use(chaiHttp)

describe('USER API',()=>{
    describe('GET/api/users',()=>{
        it("get all users",(done)=>{
            chai.request(server)
            .get("/api/user/userviews")
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("object")
                response.body.rows[0].should.have.property("id")
                response.body.rows[0].should.have.property("first_name")
                response.body.rows[0].should.have.property("last_name")
                response.body.rows[0].should.have.property("email")
                response.body.rows[0].should.have.property("phone_number")
                response.body.rows[0].should.have.property("status")
                response.body.rows[0].should.have.property("role_id")
                done()
            })
        })
    })
    it("should not get all users",(done)=>{
        chai.request(server)
        .get("/api/user/userview")
        .end((err,response)=>{
            response.should.have.status(404)
            done()
        })
    })

    describe('POST/api/users',()=>{
        it("insert all users",(done)=>{
            const user={
                first_name:"albyyyydf",
                last_name:"ay",
                email:"albyyyymhqaseardsfds@gmail.com",
                phone_number:7777222777,
                password:"Albyyyymha1234"
            }
            chai.request(server)
            .post("/api/user/signup")
            .send(user)
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("object")
                response.body.should.have.property("first_name")
                response.body.should.have.property("last_name")
                response.body.should.have.property("email")
                response.body.should.have.property("status")
                response.body.should.have.property("token")
                response.body.should.have.property("message")
                done()
            })
        })
    })
    describe('POST/api/users',()=>{
        it("user login",(done)=>{
            const user={
                email:"albyyyymhqaseard@gmail.com",
                password:"Albyyyymha1234"
            }
            chai.request(server)
            .post("/api/user/signin")
            .send(user)
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("object")
                response.body.should.have.property("id")
                response.body.should.have.property("first_name")
                response.body.should.have.property("last_name")
                response.body.should.have.property("email")
                response.body.should.have.property("status")
                response.body.should.have.property("token")
                response.body.should.have.property("message")
                done()
            })
        })
    })
    describe('DELETE/api/users',()=>{
        it("delete user by id",(done)=>{
            const id = 120;
            // console.log(id)
            chai.request(server)
            .delete('/api/user/deleteuser?id=120')
            .end((err,response)=>{
                response.should.have.status(202)
                response.body.should.have.property("message")
                done()
            })
        })
    })
    describe('PUT/api/users',()=>{
        it("user update",(done)=>{
            const id = 121;
            const user=[
                {email:"albyyyymhqaseardfgds@gmail.com"},
                {phone_number:7777727777}
            ]
            chai.request(server)
            .put('/api/user/updateuser?id=121')
            .send(user)
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.have.property("message")
                done()
            })
        })
    })
    

})