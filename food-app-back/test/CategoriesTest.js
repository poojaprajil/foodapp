let chai =require('chai')
let chaiHttp=require('chai-http')
const { response } = require('express')
let server=require('../app')

chai.should()

chai.use(chaiHttp)

describe('CATEGORIES API',()=>{
    describe('GET/api/categories',()=>{
        it("list Category",(done)=>{
            chai.request(server)
            .get("/api/categories/getcategories")
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("array")
                response.body[0].should.have.property("id")
                response.body[0].should.have.property("name")
                response.body[0].should.have.property("description")
                done()
            })
        })
    })

    describe('POST/api/categories',()=>{
        it("add category",(done)=>{
            const category={
                name:"Asian",
                description:"Asian"   
            }
            chai.request(server)
            .post("/api/categories/addcategories")
            .send(category)
            .end((err,response)=>{
                console.log(response.body)
                response.should.have.status(200)
                response.body.should.be.a("object")
                done()
            })
        })
    })


})