let chai =require('chai')
let chaiHttp=require('chai-http')
const { response } = require('express')
let server=require('../app')

chai.should()

chai.use(chaiHttp)

describe('ROLE API',()=>{
    describe('GET/api/role',()=>{
        it("get roles",(done)=>{
            chai.request(server)
            .get("/api/role/getroles")
            .end((err,response)=>{
                response.should.have.status(200)
                response.body.should.be.a("array")
                response.body[0].should.have.property("id")
                response.body[0].should.have.property("role_name")
                done()
            })
        })
    })
})