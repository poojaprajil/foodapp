const jwt = require('jsonwebtoken');
const TOKEN_SECRET = 'a5e87caeddd951eea1d197ebf6c4d9d1eb4bc1c47f491af640'

const generateAccessToken = ({data},time) => {
    // console.log(TOKEN_SECRET)
    return jwt.sign({data}, TOKEN_SECRET,{ expiresIn:time});

}

module.exports = generateAccessToken