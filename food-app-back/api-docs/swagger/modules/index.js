const role = require("./role/index")
const cart = require("./cart/index")
const categories = require("./categories/index")
const order = require("./order/index")
const user = require("./user/index")
const product=require("./product/index")
exports.paths = {
    ...order,
    ...cart,
    ...categories,
    ...role,
    ...user,
    ...product
}

