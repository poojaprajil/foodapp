const order = require("./order/index")
const cart = require("./cart/index")
const categories = require("./categories/index")
const user = require("./user/index")
const product = require("./product/index")


exports.definitions = {
    ...order,
    ...cart,
    ...categories,
    ...user,
    ...product
   
}
