const express= require('express')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger/index');

var app = express();
const bodyParser = require('body-parser')

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
const options = {
    customCss: '.swagger-ui .topbar { display: none }',
    customLevelTitle: "API Overview | swagger API Documentation"
  };
  app.use('/docs/web/api', swaggerUi.serveFiles(swaggerDocument,options), swaggerUi.setup(swaggerDocument,options));
  
  
app.listen(5001)

console.log('listening on port 5001..')
module.exports = app