var express= require('express');
const {UserSignup, userSigIn,userViews,deleteUser, updateUser, forgotPassword, resetPassword}= require('../controllers/User')
// const {usersViews,UsersSignup,usersDelete,usersUpdate}=require('../controllers/NewUser')


var router= express.Router()

router.post('/signup',UserSignup);
router.post('/signin',userSigIn);
router.get('/userviews',userViews);
router.put('/updateuser',updateUser);
router.delete('/deleteuser',deleteUser);
router.post('/forgotpassword',forgotPassword);
router.post('/resetpassword',resetPassword)

// mongo
// router.get('/usersviews',usersViews);
// router.post('/signups',UsersSignup);
// router.delete('/userdelete',usersDelete);
// router.put('/userupdate',usersUpdate);

module.exports=router