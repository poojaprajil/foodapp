var express= require('express');
const {getCategories,addCategories} = require('../controllers/Categories.js');


var router= express.Router()

router.get('/getcategories',getCategories);
router.post('/addcategories',addCategories);



module.exports=router