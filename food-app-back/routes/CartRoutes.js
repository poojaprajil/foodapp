var express= require('express');
const {addToCart,listCart, deleteCartItem,updateCart}= require('../controllers/Cart');
const listCartItems= require('../typeScriptOut/Cart');

var router= express.Router()

router.get('/listCartItems',listCartItems);
router.post('/addtocart',addToCart);
router.get('/listcart',listCart);
router.delete('/deletecartitem',deleteCartItem);
router.put('/updatecartitem',updateCart);


module.exports=router