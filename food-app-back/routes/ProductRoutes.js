var express= require('express');
const {addProduct,listProducts,updateProduct,deleteProduct} = require('../controllers/Products');
const multer=require('multer')
const path=require('path')
var router= express.Router()

const storage = multer.diskStorage({
    destination:'public/uploads',
    filename:(req,file,cb)=>{
        console.log(file,"file")
        return cb(null,`${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})
const upload= multer({storage:storage})

router.post('/addproduct',upload.single('image'),addProduct);
router.get('/listproducts',listProducts);
router.put('/updateproduct',upload.single('image'),updateProduct);
router.delete('/deleteproduct',deleteProduct);




module.exports=router