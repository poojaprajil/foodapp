const express= require('express')
var app = express();
const bodyParser = require('body-parser')
const cors = require('cors')
// const db= require('./db/NewConnection')
// const cookieParser = require("cookie-parser");





app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
const UserRoute=require('./routes/UserRoutes')
const ProductRoutes=require('./routes/ProductRoutes')
const CategoriesRoutes=require('./routes/CatogoriesRoutes')
const RoleRoutes=require('./routes/RoleRoutes')
const CartRoutes=require('./routes/CartRoutes')
const OrderRoutes=require('./routes/OrderRoutes')


// app.use(cookieParser());

app.use(cors())
app.use('/public/uploads', express.static('public/uploads'))
app.use("/api/user",UserRoute)
app.use("/api/product",ProductRoutes)
app.use("/api/categories",CategoriesRoutes)
app.use("/api/role",RoleRoutes)
app.use("/api/cart",CartRoutes)
app.use("/api/order",OrderRoutes)

app.listen(5009)

console.log('listening on port 5009..')
module.exports = app